import { useContext } from 'react';
import { AuthAdminContext } from 'context/auth-admin-context';

const useAuthAdmin = () => {
  const [state, setState] = useContext(AuthAdminContext);

  function signIn(name: string) {
    setState((state) => ({ ...state, isAuthenticated: true, name }));
  }

  function signOut() {
    setState({ ...state, isAuthenticated: false });
  }

  return { signIn, signOut, isAuthenticated: state.isAuthenticated, name: state.name };
};

export { useAuthAdmin };
