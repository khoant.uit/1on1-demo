import { BookingInfoContext } from 'context/booking-info-context';
import { RegisterFormData } from 'interfaces';
import { useContext } from 'react';
import moment from 'moment';
import { keyTime } from 'constants/Constants';

export const useBookingInfo = () => {
  const [state, setState] = useContext(BookingInfoContext);
  function addNewBooking(formData: RegisterFormData) {
    const key = moment(formData.time).format(keyTime);
    const newState = Object.assign({}, state, { [key]: formData });
    console.log(newState);
    setState(newState);
  }
  function removeBooking(time: string) {
    if (state.hasOwnProperty(time)) {
      const keyToDelete = moment(time).format(keyTime);
      const newState = Object.assign({}, { ...state });
      delete newState[keyToDelete];
      setState(newState);
    }
  }
  return { bookingInfo: state, addNewBooking, removeBooking };
};
