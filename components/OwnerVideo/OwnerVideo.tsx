import React, { useEffect, useState } from "react";
import { VideoCallContext } from "../../context/VideoCallContext/VideoCall.context";
import { OwnerVideoWrapper } from "./OwnerVideo.style";

export default function OwnerVideo() {
  const {
    videoCallState: { localVCUser },
    videoCallDispatch,
  } = React.useContext<any>(VideoCallContext);
  const [streamLoaded, setStreamLoaded] = useState(false);

  useEffect(() => {
    if (Object.keys(localVCUser).length > 0) {
      let localVideo = document.getElementById(
        "localVideo"
      ) as HTMLVideoElement;

      localVideo.onloadeddata = function () {
        setStreamLoaded(true);
        let vhownervideo = document.getElementById("localVideo").clientHeight;
        document.documentElement.style.setProperty(
          "--vhownervideo",
          `${vhownervideo}px`
        );
      };
      localVideo.srcObject = localVCUser[Object.keys(localVCUser)[0]];
    }
  }, [localVCUser]);

  return (
    <OwnerVideoWrapper>
      <video
        style={{visibility: streamLoaded ? "visible" : "collapse"}}
        id="localVideo"
        className="local-video"
        autoPlay
        muted
        playsInline
      ></video>
    </OwnerVideoWrapper>
  );
}
