import styled from "styled-components";

export const OwnerVideoWrapper = styled.div`
  width: 15%;
  height: auto;
  background: transparent;
  position: absolute;
  right: 16px;
  bottom: 16px;
  .local-video {
    width: 100%;
    height: 100%;
  }

  @media (max-width: 480px) {
    width: 25%;
    height: 30%;
    right: 10px;
    /* top: calc(var(--vh) - var(--vhownervideo));; */
    top: 10px;
    .local-video {
      width: 100%;
      height: auto;
    }
  }
`;
