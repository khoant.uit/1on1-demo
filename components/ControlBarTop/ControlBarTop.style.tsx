import styled from 'styled-components';

export const ControlBarTopWrapper = styled.div`
  width: calc(100% - 32px);
  min-height: 40px;
  position: absolute;
  left: 16px;
  top: 16px;
`;
