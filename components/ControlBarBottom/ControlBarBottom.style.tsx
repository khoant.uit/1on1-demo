import styled from "styled-components";
export const TimeCountDownBottom = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background: #2D2E36;
  width: 10vw;
  height: 50px;
  position: absolute;
  left: 50%;
  bottom: 90px;
  transform: translateX(-50%);
  border-radius: 10px;
   @media (max-width: 768px) {
    width: 20vw;
  }
  @media (max-width: 480px) {
    width: 30vw;
  }
  span{
    color: #FFF;
    font-weight: 600;
    font-size: 1.5rem;
  }
`;
export const ControlBarBottomWrapper = styled.div`
  display: flex;
  justify-content: center;
  background: #2D2E36;
  min-width: 20vw;
  padding: 0 25px;
  min-height: 80px;
  position: absolute;
  left: 50%;
  bottom: 0;
  transform: translateX(-50%);
  border-radius: 30px 30px 0px 0px;
  .div-control {
    justify-content: center;
    display: flex;
  }
  svg {
    cursor: pointer;
    margin-left: 8px;
    margin-right: 8px;
    font-size: 26px;
  }

  @media (max-width: 480px) {
    width: 90vw;
    /* .div-control {
      padding: 0 10px;
    } */
    /* svg {
      margin-left: 12px;
      margin-right: 12px;
      font-size: 44px;
    } */
  }
`;
