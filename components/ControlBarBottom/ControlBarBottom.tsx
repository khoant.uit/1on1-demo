import React, { useEffect, useState } from "react";
import { ControlBarBottomWrapper, TimeCountDownBottom } from "./ControlBarBottom.style";
import { HiPhoneMissedCall } from "react-icons/hi";
import { RiCameraSwitchFill } from "react-icons/ri";
import { AiOutlineAudio, AiOutlineAudioMuted, AiFillVideoCamera, AiFillStop } from "react-icons/ai";
import { MdScreenShare, MdStopScreenShare } from "react-icons/md";
import { VideoCallContext } from "../../context/VideoCallContext/VideoCall.context";
import VideoCallAction from "../../context/VideoCallContext/VideoCallAction";
import { BtnCustom } from "../../styles/custom.style";
import { Space, message, Modal } from "antd";
import { CameraIcon, EndCallIcon, MuteVideoIcon, MuteVoiceIcon, VideoIcon, VoiceIcon } from "../../icon/SVGIcon";

export default function ControlBarBottom() {
  const [mute, unMute] = useState(true);
  const [muteVideo, unMuteVideo] = useState(true);
  const {
    videoCallState: {
      remoteVCUsers,
      isShareScreen,
      isMobileOrTablet,
      supportFaceUser,
      isDefaultCamera,
    },
    videoCallDispatch,
  } = React.useContext<any>(VideoCallContext);

  const handleClickEndCall = () => {
    videoCallDispatch({
      type: VideoCallAction.END_VIDEO_CALL,
      item: true,
    });
  };
  const handleClickMute = (mute) => {
    videoCallDispatch({
      type: VideoCallAction.MUTE_CALL,
      item: mute,
    });
  };
  const handleClickMuteVideo = (muteVideo) => {
    videoCallDispatch({
      type: VideoCallAction.MUTE_CALL_VIDEO,
      item: muteVideo,
    });
  };

  const handleClickShareScreen = () => {
    videoCallDispatch({
      type: VideoCallAction.REQUEST_SCREEN,
      item: true,
    });
  };

  const handleSwitchCamera = () => {
    if (isDefaultCamera == null)
      videoCallDispatch({
        type: VideoCallAction.DEFAULT_CAMERA,
        item: false,
      });
    if (isDefaultCamera == true)
      videoCallDispatch({
        type: VideoCallAction.DEFAULT_CAMERA,
        item: false,
      });
    if (isDefaultCamera == false)
      videoCallDispatch({
        type: VideoCallAction.DEFAULT_CAMERA,
        item: true,
      });
  };
  // const [days, setDays] = useState(Number);
  // const [hours, setHours] = useState(Number);
  // const [minutes, setMinutes] = useState(Number);
  // const [seconds, setSeconds] = useState(Number);
  // function countdown(date) {
  //   const second = 1000,
  //       minute = second * 60,
  //       hour = minute * 60,
  //       day = hour * 24;
  //   let registerTo = date,
  //       countDown = new Date(registerTo).getTime(),
  //       x = setInterval(function () {
  //           let now = new Date().getTime(),
  //               distance = countDown - now;
  //           setDays(Math.floor(distance / day)),
  //           setHours(Math.floor((distance % day) / hour)),
  //           setMinutes(Math.floor((distance % hour) / minute)),
  //           setSeconds(Math.floor((distance % minute) / second));
  //           if (distance < 0) {
  //               clearInterval(x);
  //           }
  //       }, 0);
  // }
  const { confirm } = Modal;

  function showConfirm() {
    confirm({
      className: 'modal-confirm-end',
      centered: true,
      title: '通話を終了します。よろしいですか。',
      onOk() {
        handleClickEndCall();
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  useEffect(() => {
    handleClickMute(mute)
  }, [mute])

  useEffect(() => {
    handleClickMuteVideo(muteVideo)
  }, [muteVideo])
  
  return (
    <div>
      {/* <TimeCountDownBottom>
        <span>2:00</span>
      </TimeCountDownBottom> */}
      <ControlBarBottomWrapper>
      {Object.keys(remoteVCUsers).length > 0 && (
        <div className="div-control">
          <Space size='large'>
            <BtnCustom style={{background: mute ? '#1EA5FC' : '#8962F8'}} type='primary' icon={mute ? <VoiceIcon /> : <MuteVoiceIcon />} onClick={() => {
              handleClickMute(!mute)
              unMute(!mute)
              if (mute) {
                message.info('Mute my audio');
              } else {
                message.info('Unmute my audio');
              }
            }} />
            <BtnCustom style={{background: muteVideo ? '#1EA5FC' : '#8962F8'}} type='primary' icon={muteVideo ? <VideoIcon /> : <MuteVideoIcon />} onClick={() => {
              handleClickMuteVideo(!muteVideo)
              unMuteVideo(!muteVideo)
              if (muteVideo) {
                message.info('Mute my video');
              } else {
                message.info('Unmute my video');
              }
            }} />
            {/* {!isShareScreen && !isMobileOrTablet && (
              <BtnCustom type='primary' icon={ <MdScreenShare /> } onClick={() => handleClickShareScreen()}/>
            )} */}
            {/* {isShareScreen && !isMobileOrTablet && (
              <BtnCustom type='primary' icon={<MdStopScreenShare />} onClick={() => handleClickShareScreen()}/>
            )} */}
            {isMobileOrTablet && supportFaceUser && (
              <BtnCustom type='primary' icon={<CameraIcon />} onClick={() => handleSwitchCamera()}/>
            )}
            <BtnCustom type='primary' danger icon={<EndCallIcon />} onClick={() => showConfirm()}/>
          </Space>
        </div>
      )}
    </ControlBarBottomWrapper>
    </div>
  );
}
