import styled from 'styled-components'

export const UnOwnerVideoWrapper = styled.div`
  width: 100%;
  height: 100%;
  background: #082C46;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  .tool-video {
    svg {
      font-size: 25px;
      /* @media (max-width: 480px) {
        font-size: 20px;
      } */
    }
    p {
      color: white;
      font-size: 1.2rem;
      margin: 0;
      font-weight: 600;
      text-align: center;
      @media (max-width: 480px) {
        font-size: 1rem;
      }
    }
    width: 150px;
    padding: 5px 10px;
    position: absolute;
    top: 20px;
    left: 50%;
    transform: translateX(-50%);
    display: flex;
    background: rgba(0, 0, 0, 0.6);
    border-radius: 10px;
    justify-content: center;
    align-items: center;
    /* @media (max-width: 480px) {
      top: 5px;
      padding: 2px 10px;
    } */
  }
  .remote-video {
    width: 100%;
    height: 100%;
  }
  .container {
    position: relative;
    margin-top: 0px;
    height: calc(100vh - 160px);
    width: 100%;
    min-width: 50vw;
    /* max-width: 1024px; */
    display: flex;
    justify-content: center;
    @media (max-width: 480px) {
      height: 100vh;
    }
  }
  video {
    object-fit: contain !important;
  }
`
