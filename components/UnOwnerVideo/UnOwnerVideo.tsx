import React, { useEffect, useState } from "react";
import { VideoCallContext } from "../../context/VideoCallContext/VideoCall.context";
import VideoCallAction from "../../context/VideoCallContext/VideoCallAction";
import { UnOwnerVideoWrapper } from "./UnOwnerVideo.style";
import { AiOutlineAudio, AiOutlineAudioMuted ,AiFillVideoCamera, AiFillStop } from "react-icons/ai";
import { BtnCustom } from "../../styles/custom.style";
import { Button, message } from "antd";

type Props = {
  videoInfor: any;
  stream: any;
  peerId: any;
  handleCrashPeerId(peerId: any): void;
};

export default function UnOwnerVideo(props: Props) {

  const [mute, unMute] = useState(true);
  const [muteVideo, unMuteVideo] = useState(true);

  const {videoInfor, stream} = props;
  if (Object.keys(videoInfor).length <= 0) {
    videoInfor[props.peerId] = {
      checkIntervall: null,
      countCheck: 0,
      currentTime: 0,
      previousTime: 0
    }
  }

  function checkVideoTimeUpdate() {
    if (videoInfor[props.peerId].currentTime != videoInfor[props.peerId].previousTime) {
      videoInfor[props.peerId].previousTime = videoInfor[props.peerId].currentTime;
      videoInfor[props.peerId].countCheck = 0;
    } else videoInfor[props.peerId].countCheck = videoInfor[props.peerId].countCheck + 1;
    if (videoInfor[props.peerId].countCheck == 2) {
      let localVideo = document.getElementById(
        props.peerId
      ) as HTMLVideoElement;
      if (localVideo) {
        props.handleCrashPeerId(props.peerId);
      }
    }
  }

  useEffect(() => {
    if (props.stream && props.peerId) {
      let localVideo = document.getElementById(
        props.peerId
      ) as HTMLVideoElement;

      localVideo.onloadeddata = function () {
        videoInfor[props.peerId].checkIntervall = setInterval(checkVideoTimeUpdate, 2000);
      };

      localVideo.addEventListener("timeupdate", (event) => {
        videoInfor[props.peerId].currentTime = parseInt(event.timeStamp + "");
      });

      localVideo.srcObject = props.stream;
    }
  }, [props.stream, props.peerId]);
  
  useEffect(() => {
    stream.getAudioTracks()[0].enabled = mute;
  }, [mute, stream])

  useEffect(() => {
    stream.getVideoTracks()[0].enabled = muteVideo;
  },[muteVideo, stream])

  return (
    <UnOwnerVideoWrapper>
      <div className='container'>
        <video
          id={props.peerId}
          className="remote-video"
          autoPlay
          playsInline
        >
        </video>
        {/* <div className='tool-video'> */}
          {/* <p>{props.peerId.substr(0, 6)}</p> */}
          {/* <div>
            <Button className='btn-mute' type='text' icon={muteVideo ? <AiFillVideoCamera color='#fff' /> : <AiFillStop color='red' />} onClick={() => {
              unMuteVideo(!muteVideo);
              if (muteVideo) {
                message.info(`Mute video's ${props.peerId.substr(0, 6)}`);
              } else {
                message.info(`Unmute video's ${props.peerId.substr(0, 6)}`);
              }
            }} />
            <Button className='btn-mute' type='text' icon={mute ? <AiOutlineAudio color='#fff' /> : <AiOutlineAudioMuted color='red' />} onClick={() => {
              unMute(!mute);
              if (mute) {
                message.info(`Mute audio's ${props.peerId.substr(0, 6)}`);
              } else {
                message.info(`Unmute audio's ${props.peerId.substr(0, 6)}`);
              }
            }} />
          </div> */}
        {/* </div> */}
      </div>
    </UnOwnerVideoWrapper>
  );
}
