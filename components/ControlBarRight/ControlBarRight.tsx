import { Space } from "antd";
import React from "react";
import { VideoCallContext } from "../../context/VideoCallContext/VideoCall.context";
import { ControlBarRightWrapper } from "./ControlBarRight.style";
import UserBubble from "./UserBubble";
import { isBrowser, isMobile, isTablet } from "react-device-detect";

export default function ControlBarRight() {
  const {
    videoCallState: { remoteVCUsers },
    videoCallDispatch,
  } = React.useContext<any>(VideoCallContext);

  {Object.keys(remoteVCUsers).forEach((item) => {
  })}

  return (
    <ControlBarRightWrapper>
      <h1 style={{fontSize: isMobile && Object.keys(remoteVCUsers).length === 2 && '1rem'}}><span style={{color: '#0034df', fontSize: '1.5em'}}>大村</span>商会商談システム</h1>
      <Space>
        {Object.keys(remoteVCUsers).length <= 1 && <></>}
        {Object.keys(remoteVCUsers).length > 1 && Object.keys(remoteVCUsers).map((item, index)=> {
          return <UserBubble key={index} peerId={item + ""} isAll={false} />;
        })}
        {Object.keys(remoteVCUsers).length > 1 && (
          <UserBubble peerId={"ALL"} isAll={true} />
        )}
      </Space>
    </ControlBarRightWrapper>
  );
}
