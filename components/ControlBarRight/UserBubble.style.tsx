import styled from "styled-components";

export const UserBubbleWrapper = styled.div`
  /* width: 60px;
  height: 60px;
  background: gray;
  border-radius: 50px;
  margin-bottom: 4px; */
  width: 60px;
  height: 60px;
  border-color: transparent;
  background: ${props => props.background};
  /* background: gray; */
  border-radius: 50%;
  margin-bottom: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  transition: all .5s;
  &:hover{
    opacity: 0.8;
  }
  p {
    margin: auto;
    font-weight: bold;
    color: #fff;
  }

  @media only screen and (max-width: 1280px) {
    b {
      font-size: 10px;
    }
    span {
      font-size: 10px;
    }
  }
`;
