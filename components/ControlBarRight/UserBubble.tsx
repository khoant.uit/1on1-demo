import React from "react";
import Constant from "../../constants/Constants";
import { VideoCallContext } from "../../context/VideoCallContext/VideoCall.context";
import VideoCallAction from "../../context/VideoCallContext/VideoCallAction";
import { UserBubbleWrapper } from "./UserBubble.style";

type Props = {
  peerId: string;
  isAll: boolean;
};

function UserBubble(props: Props) {
  const {
    videoCallState: { remoteVCUsers, mainStream },
    videoCallDispatch,
  } = React.useContext<any>(VideoCallContext);

  const handleClickMainStream = () => {
    if (props.isAll == false) {
      let mainstream = {
        [Constant.PEER_MAIN]: remoteVCUsers[props.peerId],
        peerId: props.peerId,
      };
      videoCallDispatch({
        type: VideoCallAction.MAIN_STREAM,
        item: mainstream,
      });
    } else {
      videoCallDispatch({
        type: VideoCallAction.MAIN_STREAM,
        item: {},
      });
    }
  };

  return (
    <div id={props.peerId}>
      <UserBubbleWrapper
        background={
          Object.keys(mainStream).length > 0 &&
          mainStream.peerId == props.peerId
            ? "#2F97C1"
            : Object.keys(mainStream).length <= 0 && props.isAll ? "#2F97C1" : "gray" 
        }
        onClick={() => handleClickMainStream()}
      >
          <p style={props.isAll ? { fontSize: "14px" } : { fontSize: "12px" }}>
            {props.isAll == true ? "All" : props.peerId.substr(0, 6)}
          </p>
      </UserBubbleWrapper>
    </div>
  );
}
export default UserBubble;
