import styled from "styled-components";

export const ControlBarRightWrapper = styled.div`
  padding: 0 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: absolute;
  top: 0;
  background: #272B31;
  min-height: 80px;
  width: 100vw;
  max-height: calc(100% - 400px);
  overflow-y: auto;
  h1{
    color: #fff;
    font-weight: 600;
  }
  .scrollable-element {
    scrollbar-width: none;
  }
`;

