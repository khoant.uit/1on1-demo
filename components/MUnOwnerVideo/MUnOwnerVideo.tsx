import React, { useEffect, useState } from "react";
import { VideoCallContext } from "../../context/VideoCallContext/VideoCall.context";
import UnOwnerVideo from "../UnOwnerVideo/UnOwnerVideo";
import { MUnOwnerVideoWrapper } from "./MUnOwnerVideo.style";
import ReactDOM from "react-dom";
import VideoCallAction from "../../context/VideoCallContext/VideoCallAction";
import Constant from "../../constants/Constants";
import { VideoCallProvider } from "../../context/VideoCallContext/VideoCall.provider";

export default function MUnOwnerVideo() {
  const {
    videoCallState: { remoteVCUsers, addedVCUser, removeVCUser, mainStream },
    videoCallDispatch,
  } = React.useContext<any>(VideoCallContext);

  const handleCrashPeerId = (peerId) => {
    videoCallDispatch({
      type: VideoCallAction.CRASH_PEERID,
      item: peerId,
    });
  };

  useEffect(() => {
    if (Object.keys(mainStream).length > 0) {
      let vcUser = (
        <UnOwnerVideo
          videoInfor={{}}
          key={Constant.PEER_MAIN}
          handleCrashPeerId={handleCrashPeerId}
          stream={mainStream[Constant.PEER_MAIN]}
          peerId={Constant.PEER_MAIN}
        />
      );
      ReactDOM.render(
        vcUser,
        document.getElementById("video" + Constant.PEER_MAIN)
      );
    }
  }, [mainStream]);

  function addOrRemovePeerId(peerId, isAdded) {
    setTimeout(() => {
      if (isAdded) {
        if (!document.getElementById("video" + peerId)) {
          let container = document.getElementById("remote-video-container");
          let remoteVideo = document.createElement("div");
          remoteVideo.classList.add('remote-video-content')
          remoteVideo.id = "video" + peerId;
          remoteVideo.style.width = "0px";
          container.appendChild(remoteVideo);
          setWidthVideo();

          let vcUser = (
            <UnOwnerVideo
              videoInfor={{}}
              key={peerId}
              handleCrashPeerId={handleCrashPeerId}
              stream={remoteVCUsers[peerId]}
              peerId={peerId}
            />
          );
          ReactDOM.render(vcUser, document.getElementById("video" + peerId));
        }
      } else {
        let container = document.getElementById("remote-video-container");
        let component = document.getElementById("video" + peerId);
        if (component) container.removeChild(component);
        setWidthVideo();
      }
    }, Object.keys(remoteVCUsers).length * 1000);
  }

  function setWidthVideo() {
    Object.keys(remoteVCUsers).forEach((item) => {
      let component = document.getElementById("video" + item);
      if (component)
        component.style.width = 100 / Object.keys(remoteVCUsers).length + "%";
    });
  }

  useEffect(() => {
    if (addedVCUser) {
      addOrRemovePeerId(addedVCUser, true);
      videoCallDispatch({
        type: VideoCallAction.ADDED_VCUSER,
        item: null,
      });
    }
    if (removeVCUser) {
      addOrRemovePeerId(removeVCUser, false);
      videoCallDispatch({
        type: VideoCallAction.REMOVE_VCUSER,
        item: null,
      });
    }
  }, [remoteVCUsers, addedVCUser, removeVCUser]);

  return (
    <MUnOwnerVideoWrapper>
      {Object.keys(remoteVCUsers).length <= 0 && (
        <div className="div-no-one">誰かがこの部屋に加わるのを待っています！</div>
      )}
      <div
        id="remote-video-container"
        className="remote-video-container"
        style={{
          display:
            Object.keys(mainStream).length <= 0 &&
            Object.keys(remoteVCUsers).length > 0
              ? "flex"
              : "none",
          visibility:
            Object.keys(mainStream).length <= 0 &&
            Object.keys(remoteVCUsers).length > 0
              ? "visible"
              : "collapse",
        }}
      ></div>
      {Object.keys(mainStream).length > 0 && (
        <div
          id={"video" + Constant.PEER_MAIN}
          style={{
            height: "100%",
            width: "100%",
            visibility:
              Object.keys(mainStream).length <= 0 ? "collapse" : "visible",
          }}
        >
          {/* <UnOwnerVideo
            key={Constant.PEER_MAIN}
            handleCrashPeerId={handleCrashPeerId}
            stream={mainStream[Constant.PEER_MAIN]}
            peerId={Constant.PEER_MAIN}
          /> */}
        </div>
      )}
    </MUnOwnerVideoWrapper>
  );
}
