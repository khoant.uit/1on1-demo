import styled from "styled-components";

export const MUnOwnerVideoWrapper = styled.div`
  background: transparent;
  left: 0px;
  top: 0px;
  right: 0px;
  bottom: 0px;
  position: absolute;
  #remote-video-container{
    justify-content: flex-start;
  }
  .remote-video-container {
    background: transparent;
    display: flex;
    left: 0px;
    top: 0px;
    right: 0px;
    bottom: 0px;
    position: absolute;
  }

  .div-no-one {
    color: white;
    font-size: 20px;
    position: absolute;
    left: 50%;
    top: 20vh;
    transform: translateX(-50%);
    /* background-image: url("./user.png"); */
    height: 340px;
    width: 604px;
    background-repeat: no-repeat;
    background-size: contain;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    padding-bottom: 20px;
    @media (max-width: 480px) {
      width: 300px;
      height: 180px;
    }
    @media (max-height: 480px) {
      width: 300px;
      height: 180px;
      font-size: 15px;
    }
  }
  @media (max-width: 480px) {
    position: unset;
    height: var(--vh);
    display: flex;

    .remote-video-container {
      position: unset;
      flex-direction: column;
      justify-content: center;
      .remote-video-content{
        width: 100% !important;
        height: auto;
      }
    }
  }
`;
