import styled from 'styled-components';

type props = {
  text: string;
  subText: string;
  isColor?: boolean;
  onClick: (e) => void;
};

export default function BoxButton({ text, subText, isColor = false, onClick }: props) {
  return (
    <Wrapper isColor={isColor} onClick={onClick}>
      <span className='large-text'>{text}</span>
      <span className='sub-text'>{subText}</span>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  cursor: pointer;
  width: 17vw;
  height: 22vh;
  background: ${(props) =>
    props.isColor
      ? `linear-gradient(90.57deg, #4045c1 27.61%, #9799dd 109.62%, #ffffff 109.63%)`
      : `rgba(0, 0, 5, 0.8)`};
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: white;
  .large-text {
    /* font-size: 30px; */
    font-size: 2rem;
  }
  .sub-text {
    /* font-size: 20px; */
    font-size: 1.5rem;
  }
`;
