import styled from 'styled-components';

const Wrapper = styled.div`
  min-height: 100vh;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  background-attachment: fixed;
  .black-session {
    position: relative;
    margin-top: 2%;
    background: rgba(0, 0, 0, 0.7);
    border-radius: 15px;
    padding: 40px;
    /* min-height: 70vh; */
    height: 75vh;
    display: flex;
    flex-direction: column;
    /* justify-content: center; */
  }
  .steps-content {
    overflow: auto;
  }
  .steps-action {
    position: absolute;
    bottom: 5%;
    right: 3%;
    display: flex;
    justify-content: flex-end;
    /* background: rgba(0, 0, 0, 0.7); */
    /* border-radius: 20px; */
  }
  .ant-steps-item-title {
    color: white !important;
    font-size: 1.5rem;
  }
  .ant-steps-icon {
    font-size: 1.4rem;
  }
  .ant-typography {
    color: white;
  }
  .ant-steps {
    width: 70%;
    margin-left: auto;
    margin-right: auto;
  }
`;

export { Wrapper };
