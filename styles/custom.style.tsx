import { Button } from 'antd';
import styled from 'styled-components';

export const BtnCustom = styled(Button)`
    border-radius: 50%;
    width: 50px;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #1EA5FC;
    border-color: transparent;
    box-shadow: rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px;
    &:hover, &:focus{
        background-color: #1EA5FC;
        border-color: transparent;
        opacity: 0.8;
    }
`;