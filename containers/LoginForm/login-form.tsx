import { Form, Input, Button, Row, Col, message } from 'antd';
import { CSSProperties, useState } from 'react';
import styled from 'styled-components';
import { useAuthAdmin } from 'hooks/use-auth-admin';

type props = {
  // onLoginSuccess: () => void;
  title: string;
  style?: CSSProperties;
};
export default function LoginForm({ style, title }: props) {
  const { signIn } = useAuthAdmin();
  const [loading, setLoading] = useState(false);
  const onFinish = (values) => {
    setLoading(true);
    if (values.username !== 'admin@gmail.com' || values.password !== 'admin') {
      message.error('login fail');
      setLoading(false);
      return;
    }
    setTimeout(() => {
      message.success('ログインに成功しました。');
    }, 1000);
    setTimeout(() => {
      setLoading(false);
      signIn('admin');
    }, 2000);
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Wrapper style={style}>
      <Row style={{ width: '100%' }} justify='center'>
        <Col span={20} md={8}>
          <Form
            layout='vertical'
            className='form-wrapper'
            name='basic'
            labelAlign='left'
            labelCol={{
              span: 6,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}>
            <p className='title'>{title}</p>
            <p className='sub-title'>WEBツアー予約管理システム</p>
            <Form.Item
              label='ユーザID'
              name='username'
              rules={[
                {
                  required: true,
                  message: 'Please input your username!',
                },
              ]}>
              <Input placeholder='管理者ID' />
            </Form.Item>

            <Form.Item
              label='パスワード'
              name='password'
              rules={[
                {
                  required: true,
                  message: 'Please input your password!',
                },
              ]}>
              <Input.Password />
            </Form.Item>

            {/* <Form.Item
              name='remember'
              valuePropName='checked'
              wrapperCol={{
                offset: 4,
                span: 16,
              }}>
              <Checkbox>Remember me</Checkbox>
            </Form.Item> */}

            <Form.Item
              wrapperCol={{
                offset: 3,
                span: 18,
              }}
              style={{ marginBottom: 0 }}>
              <Button type='primary' htmlType='submit' loading={loading}>
                ログインする
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  font-size: 15px !important;
  background: url('/bg-register.png');
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  background-attachment: fixed;
  height: 100vh;
  width: 100vw;
  display: flex;
  justify-content: center;
  align-items: center;
  label,
  input,
  button {
    font-size: 15px;
    color: white;
  }
  input {
    color: black;
  }
  button {
    width: 100%;
    margin-left: auto;
    margin-right: auto;
  }
  .title,
  .sub-title {
    color: white;
    font-size: 20px;
    text-align: center;
  }
  .title {
    font-size: 30px;
    margin-bottom: 0;
  }
  .form-wrapper {
    padding: 5% 10%;
    background: rgba(0, 0, 0, 0.7);
    /* display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center; */
    border-radius: 10px;
  }
`;
