import { Calendar, Row, Col, Tag, Typography, Modal, Card, Divider, Empty } from 'antd';
import moment, { Moment } from 'moment';
import React, { useEffect, useState } from 'react';
import { CSSProperties } from 'react';
import styled from 'styled-components';
import { useBookingInfo } from 'hooks/use-booking-info';
import { CheckCircleTwoTone, CloseCircleTwoTone } from '@ant-design/icons';
import { keyTime } from 'constants/Constants';
import { RegisterFormData } from 'interfaces';

const { Title, Paragraph } = Typography;
type props = {
  style?: CSSProperties;
  value?: Date;
  onChange?: (values: Date) => void;
};
type ItemTime = { label: string; value: Moment; type?: 'full' | 'part-full' };
export default function AdminCalendar(props: props) {
  const { value: valueExternal, onChange: onChangeExternal, style } = props;
  const [internalValue, setInternalValue] = useState<Date>(
    moment().hour(10).startOf('hour').toDate()
  );
  const [dataTime, setDataTime] = useState<ItemTime[]>([]);
  const [visibleModal, setVisbleModal] = useState(false);
  const { bookingInfo } = useBookingInfo();
  const [modalBookingData, setModalBookingData] = useState<RegisterFormData | undefined>({
    time: '',
    type: 'first_time',
    name: { first: '', last: '' },
    tel: '',
    address: '',
    content: '',
  });
  const [url, setUrl] = useState('');

  useEffect(() => {
    let dataTime: ItemTime[] = [];
    for (let i = 10; i < 18; i++) {
      const time1 = moment().hour(i).startOf('hour');
      const time2 = moment().hour(i).minute(30).startOf('minute');
      const item1: ItemTime = {
        label:
          moment(time1).format('HH:mm') + ' - ' + moment(time1).add(30, 'minutes').format('HH:mm'),
        value: time1,
      };
      const item2: ItemTime = {
        label:
          moment(time2).format('HH:mm') + ' - ' + moment(time2).add(30, 'minutes').format('HH:mm'),
        value: time2,
      };
      dataTime = dataTime.concat([item1, item2]);
    }
    setDataTime(dataTime);
  }, []);

  const onChangeDate = (date: Moment) => {
    const currentSelectedHour = moment(internalValue).hour();
    const currentSelectedMinute = moment(internalValue).minute();
    const newDate = moment(date)
      .hour(currentSelectedHour)
      .minute(currentSelectedMinute)
      .startOf('minute');
    setInternalValue(newDate.toDate());
    !!onChangeExternal && onChangeExternal(newDate.toDate());
  };
  const onChangeTime = (time: Moment) => {
    const currentSelectedHour = moment(time).hour();
    const currentSelectedMinute = moment(time).minute();
    const newDate = moment(internalValue)
      .hour(currentSelectedHour)
      .minute(currentSelectedMinute)
      .startOf('minute');
    setInternalValue(newDate.toDate());
    !!onChangeExternal && onChangeExternal(newDate.toDate());
    const isBooked = bookingInfo.hasOwnProperty(moment(time).format(keyTime));
    isBooked ? showBookingInfo(time) : showBookingInfo(undefined);
  };
  const showBookingInfo = (time: Moment | undefined) => {
    if (!time) {
      setModalBookingData(undefined);
    } else {
      const key = moment(time).format(keyTime);
      if (bookingInfo.hasOwnProperty(key)) {
        setModalBookingData(bookingInfo[key]);
      }
      setUrl(window.location.origin + '/videocall?roomid=' + key);
    }
    setVisbleModal(true);
  };

  return (
    <>
      <Modal
        width='45vw'
        cancelButtonProps={{ style: { display: 'none' } }}
        maskClosable
        visible={visibleModal}
        onOk={() => setVisbleModal(false)}
        onCancel={() => setVisbleModal(false)}>
        {!modalBookingData ? (
          <Empty />
        ) : (
          <Row>
            <Col span={24} className='border-round'>
              <Row>
                <Col span={6}>
                  <Label>{labelData[0]}</Label>
                </Col>
                <Col span={18}>
                  <Para>{`${moment(modalBookingData.time).format(
                    'YYYY年MM月DD日 HH:mm'
                  )} ~ ${moment(modalBookingData.time).add(30, 'minutes').format('HH:mm')}`}</Para>
                </Col>
              </Row>
              <Divider />
              <Row>
                <Col span={6}>
                  <Label>{labelData[1]}</Label>
                </Col>
                <Col span={18}>
                  <Para>{modalBookingData.type === 'first_time' ? '初参加' : '二回目以上'}</Para>
                </Col>
              </Row>
              <Divider />
              <Row>
                <Col span={6}>
                  <Label>{labelData[2]}</Label>
                </Col>
                <Col span={18}>
                  <Para>
                    {modalBookingData.name.first}
                    {modalBookingData.name.last}
                  </Para>
                </Col>
              </Row>
              <Divider />
              <Row>
                <Col span={6}>
                  <Label>{labelData[3]}</Label>
                </Col>
                <Col span={18}>
                  <Para>{modalBookingData.tel}</Para>
                </Col>
              </Row>
              <Divider />
              <Row>
                <Col span={6}>
                  <Label>{labelData[4]}</Label>
                </Col>
                <Col span={18}>
                  <Para>{modalBookingData.address}</Para>
                </Col>
              </Row>
              <Divider />
              <Row>
                <Col span={6}>
                  <Label>{labelData[5]}</Label>
                </Col>
                <Col span={18}>
                  <Para>{modalBookingData.content}</Para>
                </Col>
              </Row>
              <Divider />
              <Row>
                <Col span={6}>
                  <Label>{labelData[6]}</Label>
                </Col>
                <Col span={18}>
                  <Para>
                    <a href={url} target='_blank'>
                      {url}
                    </a>
                  </Para>
                </Col>
              </Row>
            </Col>
          </Row>
        )}
      </Modal>
      <Wrapper style={style}>
        <Row>
          <Col span={24} md={12}>
            <Title level={3} style={{ textAlign: 'center' }}>
              日にちを選ぶ
            </Title>
            <Calendar
              dateFullCellRender={(date) => (
                <DateCell
                  style={
                    moment(internalValue).dayOfYear() === moment(date).dayOfYear()
                      ? {
                          backgroundColor: '#108ee9',
                          color: 'white',
                        }
                      : {}
                  }>
                  {moment(date).date()}
                </DateCell>
              )}
              style={{ background: 'transparent' }}
              value={valueExternal ? moment(valueExternal) : moment(internalValue)}
              onChange={onChangeDate}
            />
          </Col>
          <Col span={24} md={12}>
            <Title level={3} style={{ textAlign: 'center' }}>
              時間帯を選択した上、予約詳細は表示されます。
            </Title>
            <Row>
              <Col span={12} className='time-column'>
                {dataTime.slice(0, 8).map((time, index) => {
                  const isBooked = bookingInfo.hasOwnProperty(moment(time.value).format(keyTime));
                  return (
                    <Tag
                      color={
                        moment(valueExternal ?? internalValue).hour() ===
                          moment(time.value).hour() &&
                        moment(valueExternal ?? internalValue).minute() ===
                          moment(time.value).minute()
                          ? '#108ee9'
                          : undefined
                      }
                      style={{ cursor: 'pointer' }}
                      key={index}
                      onClick={() => onChangeTime(time.value)}>
                      {isBooked ? (
                        <CheckCircleTwoTone />
                      ) : (
                        <CloseCircleTwoTone twoToneColor='red' />
                      )}
                      {time.label}
                    </Tag>
                  );
                })}
              </Col>
              <Col span={12} className='time-column'>
                {dataTime.slice(8, 16).map((time, index) => {
                  const isBooked = bookingInfo.hasOwnProperty(moment(time.value).format(keyTime));
                  return (
                    <Tag
                      color={
                        moment(valueExternal ?? internalValue).hour() ===
                          moment(time.value).hour() &&
                        moment(valueExternal ?? internalValue).minute() ===
                          moment(time.value).minute()
                          ? '#108ee9'
                          : undefined
                      }
                      style={{ cursor: 'pointer' }}
                      key={index}
                      onClick={() => onChangeTime(time.value)}>
                      {isBooked ? (
                        <CheckCircleTwoTone />
                      ) : (
                        <CloseCircleTwoTone twoToneColor='red' />
                      )}
                      {time.label}
                    </Tag>
                  );
                })}
              </Col>
            </Row>
          </Col>
        </Row>
      </Wrapper>
    </>
  );
}
const labelData = ['日時', 'タイプ', '氏名', 'TEL.', 'メールアドレス', '備考', 'URL'];

const DateCell = styled.div`
  color: black;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  margin-left: auto;
  margin-right: auto;
`;

const Wrapper = styled.div`
  .ant-picker-calendar-header {
    padding-top: 0;
  }
  .time-column {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  .ant-picker-panel {
    background-color: transparent;
  }
  .ant-picker-content th {
    color: black;
    text-transform: uppercase;
    text-align: center;
    padding-right: 0px !important;
    font-size: 15px;
    font-weight: 700;
  }
  .ant-typography {
    color: black !important;
  }
  .ant-tag {
    font-size: 15px;
    width: 11vw;
    height: 3.7vh;
    margin-bottom: 10px;
    font-weight: 400;
    color: black;
    background-color: transparent;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;
const Label = styled(Paragraph)`
  font-weight: 700;
  margin-bottom: 0 !important;
`;
const Para = styled(Paragraph)`
  margin-bottom: 0 !important;
`;
