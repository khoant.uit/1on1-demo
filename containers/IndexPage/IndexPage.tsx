import { Button, Col, Input, message, Row, Typography, Tooltip } from "antd";
import React, { useState } from "react";
import { IndexPageWrapper } from "./IndexPage.style";
import { isBrowser, isMobile, isTablet } from "react-device-detect";
const { Text } = Typography;

export default function IndexPage() {
  const [meetingUrl, setMeetingUrl] = useState(null);
  const [hide, setHide] = useState(true)
  const handleClickGenerateUrl = () => {
    let roomId = Math.random().toString().substr(3, 6);
    let url = window.location.origin + "/videocall?roomid=" + roomId;
    setMeetingUrl(url);
    setHide(false)
  };

  const handleCopyMeetingUrl = () => {
    navigator.clipboard.writeText(meetingUrl);
    // message.success("リンクコピー済み", 2);
    message.success("Copied Successfully!", 2);
  };
  const handleOpenUrl = () => {
    window.open(meetingUrl);
  };

  return (
    <IndexPageWrapper>
      <div className="div-container">
        <h1><span style={{color: '#fff', fontSize: '1.5em'}}>大村</span>商会商談システム</h1>
        <div className="div-meeting">
          {hide ?
            <Button
            style={{ height: "40px",}}
            type="primary"
            onClick={handleClickGenerateUrl}
            >
                会議URLを生成する
            </Button>
            : (
            <div className="div-meeting-generate">
              <Row className='meeting-url' style={{ justifyContent: isMobile && 'center' }}>
                <Col span="24" style={{display: 'flex'}}>
                    <Input readOnly value={meetingUrl} onClick={handleCopyMeetingUrl} />
                    <Button type="primary" style={{fontWeight: 500}} onClick={handleOpenUrl}>会議に参加する</Button>
                </Col>
              </Row>
              <Row><span style={{marginTop: "8px", color: "#d9363e", fontWeight: 500}}>会議URLをコピーして相手に共有して下さい。</span></Row>
            </div>
          )}
        </div>
      </div>
    </IndexPageWrapper>
  );
}
