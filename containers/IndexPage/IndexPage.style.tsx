import styled from "styled-components";

export const IndexPageWrapper = styled.div`
  width: 100%;
  height: 100vh;
  position: relative;
  background-image: url('/bg-7.png');
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  @media (max-width: 480px) {
    background-image: url('/bg-9.png');
  }
  input {
    :hover {
      cursor: pointer;
    }
  }

  .div-container {
    position: absolute;
    left: 8%;
    top: 50%;
    transform: translate(-8%, -50%);
    @media (max-width: 1024px) {
      border-radius: 25px;
      left: 5%;
      width: 35vw;
    }
    @media (max-width: 480px){
      top: 20%;
      left: 15%;
      width: 90vw;
    }
  }

  h1 {
    font-weight: 600;
    text-align: center;
    font-size: 3rem;
    color: #fff;
    @media (max-width: 1024px) {
      text-align: left;
      font-size: 3rem;
    }
    @media (max-height: 480px) {
      font-size: 2.5rem;
    }
    @media (max-width: 480px) {
      font-size: 2.5rem;
    }
    &::after{
      content: "";
      display: block;
      background: #0034df;
      width: 70%;
      height: 7px;
      @media (max-width: 1024px) {
        display: none;
      }
    }
  }
  .div-meeting {
    margin-top: 15%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    @media (max-height: 480px) {
      margin-top: 0;
    }
    @media (max-width: 1024px) {
      margin-top: 5%;
    }
    .div-meeting-generate {
      width: 100%;
      .meeting-url{
        background: #fff;
        padding: 5px;
        border-radius: 5px;
      }
      .ant-row{
        justify-content: unset;
        .ant-btn-text{
          @media (max-width: 480px) {
            margin: auto;
          }
        }
      }
    }
  }
`;
