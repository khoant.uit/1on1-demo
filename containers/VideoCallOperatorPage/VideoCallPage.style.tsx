import styled from "styled-components";

export const VideoCallWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  /* height: var(--vh); */
  position: relative;
  /* min-width: 960px; */
  background: black;
  
  .div-end-call {
    color: white;
    font-size: 20px;
    position: absolute;
    left: 50%;
    top: 40%;
    transform: translate(-50%, 0);
  }
  #videomainstream{
    .container{
      min-width: 100vw;
    }
  }
  @media (max-width: 480px) {
    /* min-height: var(--vh); */
    /* position: unset; */
     #videomainstream{
      .tool-video{
        top: 180px;
      }
    }
  }
`;
