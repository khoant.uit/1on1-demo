import { Calendar, Row, Col, Tag, Typography } from 'antd';
import moment, { Moment } from 'moment';
import { useEffect, useState } from 'react';
import { CSSProperties } from 'react';
import styled from 'styled-components';
import React from 'react';
import { useBookingInfo } from 'hooks/use-booking-info';
import { CloseCircleTwoTone, CheckCircleTwoTone } from '@ant-design/icons';
import { keyTime } from 'constants/Constants';

const { Title } = Typography;
type props = {
  style?: CSSProperties;
  value?: Date;
  onChange?: (values: Date) => void;
  admin?: boolean;
};
type ItemTime = { label: string; value: Moment; type?: 'full' | 'part-full' };
export default function RegisterStep1(props: props) {
  const { bookingInfo } = useBookingInfo();
  const { value: valueExternal, onChange: onChangeExternal, style, admin = false } = props;
  const [internalValue, setInternalValue] = useState<Date>(
    moment().hour(10).startOf('hour').toDate()
  );
  const [dataTime, setDataTime] = useState<ItemTime[]>([]);
  useEffect(() => {
    let dataTime: ItemTime[] = [];
    for (let i = 10; i < 18; i++) {
      const time1 = moment().hour(i).startOf('hour');
      const time2 = moment().hour(i).minute(30).startOf('minute');
      const item1: ItemTime = {
        label:
          moment(time1).format('HH:mm') + ' - ' + moment(time1).add(30, 'minutes').format('HH:mm'),
        value: time1,
      };
      const item2: ItemTime = {
        label:
          moment(time2).format('HH:mm') + ' - ' + moment(time2).add(30, 'minutes').format('HH:mm'),
        value: time2,
      };
      dataTime = dataTime.concat([item1, item2]);
    }
    setDataTime(dataTime);
  }, []);

  const onChangeDate = (date: Moment) => {
    const currentSelectedHour = moment(internalValue).hour();
    const currentSelectedMinute = moment(internalValue).minute();
    const newDate = moment(date)
      .hour(currentSelectedHour)
      .minute(currentSelectedMinute)
      .startOf('minute');
    setInternalValue(newDate.toDate());
    !!onChangeExternal && onChangeExternal(newDate.toDate());
  };
  const onChangeTime = (time: Moment) => {
    const currentSelectedHour = moment(time).hour();
    const currentSelectedMinute = moment(time).minute();
    const newDate = moment(internalValue)
      .hour(currentSelectedHour)
      .minute(currentSelectedMinute)
      .startOf('minute');
    setInternalValue(newDate.toDate());
    !!onChangeExternal && onChangeExternal(newDate.toDate());
  };

  return (
    <Wrapper style={style} admin={admin}>
      <Row>
        <Col span={24} md={12}>
          <Title level={3} style={{ textAlign: 'center' }}>
            日にちを選ぶ
          </Title>
          <Calendar
            dateFullCellRender={(date) => (
              <DateCell
                admin={admin}
                style={
                  moment(internalValue).dayOfYear() === moment(date).dayOfYear()
                    ? {
                        backgroundColor: admin ? '#108ee9' : 'white',
                        color: admin ? 'white' : 'black',
                      }
                    : {}
                }>
                {moment(date).date()}
              </DateCell>
            )}
            style={{ background: 'transparent' }}
            value={valueExternal ? moment(valueExternal) : moment(internalValue)}
            onChange={onChangeDate}
          />
        </Col>
        <Col span={24} md={12}>
          <Title level={3} style={{ textAlign: 'center' }}>
            時間帯選択
          </Title>
          <Row>
            <Col span={12} className='time-column'>
              {dataTime.slice(0, 8).map((time, index) => {
                const isBooked = bookingInfo.hasOwnProperty(moment(time.value).format(keyTime));
                return (
                  <Tag
                    color={
                      moment(valueExternal ?? internalValue).hour() === moment(time.value).hour() &&
                      moment(valueExternal ?? internalValue).minute() ===
                        moment(time.value).minute()
                        ? '#108ee9'
                        : undefined
                    }
                    style={{
                      cursor: isBooked ? 'not-allowed' : 'pointer',
                      color:
                        admin &&
                        moment(valueExternal ?? internalValue).hour() ===
                          moment(time.value).hour() &&
                        moment(valueExternal ?? internalValue).minute() ===
                          moment(time.value).minute()
                          ? 'white'
                          : undefined,
                    }}
                    key={index}
                    onClick={() => {
                      isBooked ? undefined : onChangeTime(time.value);
                    }}>
                    {isBooked ? <CloseCircleTwoTone twoToneColor='red' /> : <CheckCircleTwoTone />}
                    {time.label}
                  </Tag>
                );
              })}
            </Col>
            <Col span={12} className='time-column'>
              {dataTime.slice(8, 16).map((time, index) => {
                const isBooked = bookingInfo.hasOwnProperty(moment(time.value).format(keyTime));
                return (
                  <Tag
                    color={
                      moment(valueExternal ?? internalValue).hour() === moment(time.value).hour() &&
                      moment(valueExternal ?? internalValue).minute() ===
                        moment(time.value).minute()
                        ? '#108ee9'
                        : undefined
                    }
                    style={{ cursor: isBooked ? 'not-allowed' : 'pointer' }}
                    key={index}
                    onClick={() => {
                      isBooked ? undefined : onChangeTime(time.value);
                    }}>
                    {isBooked ? <CloseCircleTwoTone twoToneColor='red' /> : <CheckCircleTwoTone />}
                    {time.label}
                  </Tag>
                );
              })}
            </Col>
          </Row>
          {/* <Row justify='center'>
            <Col span={24} md={6}>
              <Title level={4} style={{ textAlign: 'center' }}>
                AM
              </Title>
              <Row>
                <Col
                  span={24}
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    alignContent: 'center',
                    gap: 5,
                  }}>
                  {dataTimeAM.map((time, index) => (
                    <Tag
                      color={
                        moment(valueExternal ?? internalValue).hour() ===
                          moment(time.value).hour() &&
                        moment(valueExternal ?? internalValue).minute() ===
                          moment(time.value).minute()
                          ? '#108ee9'
                          : undefined
                      }
                      style={{ cursor: 'pointer' }}
                      key={index}
                      onClick={() => onChangeTime(time.value)}>
                      {time.label}
                    </Tag>
                  ))}
                </Col>
              </Row>
            </Col>
            <Col span={24} md={18}>
              <Title level={4} style={{ textAlign: 'center' }}>
                PM
              </Title>
              <Row>
                <Col
                  span={24}
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    height: 200,
                    flexWrap: 'wrap',
                    alignContent: 'center',
                    gap: 5,
                  }}>
                  {dataTimePM.map((time, index) => (
                    <Tag
                      color={
                        moment(valueExternal ?? internalValue).hour() ===
                          moment(time.value).hour() &&
                        moment(valueExternal ?? internalValue).minute() ===
                          moment(time.value).minute()
                          ? '#108ee9'
                          : undefined
                      }
                      style={{ cursor: 'pointer' }}
                      key={index}
                      onClick={() => onChangeTime(time.value)}>
                      {time.label}
                    </Tag>
                  ))}
                </Col>
              </Row>
            </Col>
          </Row> */}
        </Col>
      </Row>
    </Wrapper>
  );
}

const dataTimeAM: { label: string; value: Moment; type?: 'full' | 'part-full' }[] = [
  { label: '10:00 - 10:30', value: moment().hour(10).startOf('hour'), type: 'full' },
  {
    label: '10:30 - 11:00',
    value: moment().hour(10).minute(30).startOf('minute'),
  },
  { label: '11:00 - 11:30', value: moment().hour(11).startOf('hour'), type: 'part-full' },
  {
    label: '11:30 - 12:00',
    value: moment().hour(11).minute(30).startOf('minute'),
    type: 'full',
  },
];
const dataTimePM: { label: string; value: Moment }[] = [
  { label: '12:00 - 12:30', value: moment().hour(12).startOf('hour') },
  {
    label: '12:30 - 13:00',
    value: moment().hour(12).minute(30).startOf('minute'),
  },
  { label: '13:00 - 13:30', value: moment().hour(13).startOf('hour') },
  {
    label: '13:30 - 14:00',
    value: moment().hour(13).minute(30).startOf('minute'),
  },
  { label: '14:00 - 14:30', value: moment().hour(14).startOf('hour') },
  {
    label: '14:30 - 15:00',
    value: moment().hour(14).minute(30).startOf('minute'),
  },
  { label: '15:00 - 15:30', value: moment().hour(15).startOf('hour') },
  {
    label: '15:30 - 16:00',
    value: moment().hour(15).minute(30).startOf('minute'),
  },
  { label: '16:00 - 16:30', value: moment().hour(16).startOf('hour') },
  {
    label: '16:30 - 17:00',
    value: moment().hour(16).minute(30).startOf('minute'),
  },
  { label: '17:00 - 17:30', value: moment().hour(17).startOf('hour') },
  {
    label: '17:30 - 18:00',
    value: moment().hour(17).minute(30).startOf('minute'),
  },
];

const DateCell = styled.div`
  color: ${(props) => (props.admin ? 'black' : 'white')};
  width: 40px;
  height: 40px;
  border-radius: 50%;
  /* border: 1px solid yellow; */
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  margin-left: auto;
  margin-right: auto;
  /* background-color: yellow; */
`;

const Wrapper = styled.div`
  .ant-picker-calendar-date-content {
    height: 50px !important;
  }
  .ant-picker-calendar-date-value {
    color: white;
  }
  .ant-picker-calendar-header {
    padding-top: 0;
  }
  .time-column {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  .ant-picker-panel {
    background-color: transparent;
  }
  .ant-picker-content th {
    color: ${(props) => (props.admin ? 'black' : 'white')};
    text-transform: uppercase;
    text-align: center;
    padding-right: 0px !important;
    /* font-size: 15px; */
    font-size: 1.1rem;
    font-weight: 700;
  }
  .ant-tag {
    font-size: 15px;
    width: 11vw;
    height: 3.7vh;
    margin-bottom: 10px;
    font-weight: 400;
    color: ${(props) => (props.admin ? 'black' : 'white')};
    background-color: transparent;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 0.9rem 1rem;
  }
`;
