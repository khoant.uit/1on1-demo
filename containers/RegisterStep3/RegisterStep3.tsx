import { CSSProperties } from 'react';
import styled from 'styled-components';
import { Row, Col } from 'antd';
import { RegisterFormData } from 'interfaces';
import moment from 'moment';
import { keyTime } from 'constants/Constants';

type props = {
  style?: CSSProperties;
  data: RegisterFormData;
};
export default function RegisterStep3({ style, data }: props) {
  const roomId = moment(data.time).format(keyTime);
  const url = window.location.origin + '/videocall?roomid=' + roomId;

  return (
    <Wrapper style={style}>
      <Row justify='center'>
        <Col span={18}>
          <BorderBox style={{ textAlign: 'center', paddingTop: 10 }}>
            <p>ご予約完了</p>
            <p>予約時間になりましたら以下のツアーURLからアクセスをお願いします。</p>
            <p>{`予約時間：${moment(data.time).format('YYYY年MM月DD日 HH:mm ~ ')}${moment(data.time)
              .add(30, 'minutes')
              .format('HH:mm')}`}</p>
          </BorderBox>
          <BorderBox
            style={{
              backgroundColor: '#5A76FC',
              textAlign: 'center',
              marginTop: '2rem',
              marginBottom: '2rem',
              padding: '1% 0',
              borderWidth: 0,
            }}>
            <a target='_blank' href={url} style={{ marginBottom: 0 }}>
              {url}
            </a>
          </BorderBox>
          <BorderBox style={{ padding: '10px 20px 0', textAlign: 'center' }}>
            <p>ツアーURLはご登録のメールアドレスにも送付致しました。</p>
            <p>念のため、メールが送付されてることを確認してから本ページを閉じてください。</p>
            <p style={{ color: 'red' }}>ツアーURLを紛失した場合はツアーに参加できません。</p>
          </BorderBox>
        </Col>
      </Row>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  color: white;
  p {
    /* font-size: 20px; */
  }
  a {
    text-decoration: none;
    color: white;
    /* font-size: 15px; */
    &:hover {
      text-decoration: underline;
    }
  }
`;
const BorderBox = styled.div`
  border: 1px solid white;
  border-radius: 8px;
`;
