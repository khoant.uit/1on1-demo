import { useEffect, useState } from "react";
import ControlBarBottom from "../../components/ControlBarBottom/ControlBarBottom";
import ControlBarTop from "../../components/ControlBarTop/ControlBarTop";
import MUnOwnerVideo from "../../components/MUnOwnerVideo/MUnOwnerVideo";
import OwnerVideo from "../../components/OwnerVideo/OwnerVideo";
import { VideoCallWrapper } from "./VideoCallPage.style";
import { VideoCallContext } from "../../context/VideoCallContext/VideoCall.context";
import React from "react";
import { useRouter } from "next/router";
import VideoCallAction from "../../context/VideoCallContext/VideoCallAction";
import Peer from "peerjs";
import TypeMsg from "../../constants/TypeMsg";
import ControlBarRight from "../../components/ControlBarRight/ControlBarRight";
import Constant from "../../constants/Constants";
import { isBrowser, isMobile, isTablet } from "react-device-detect";

let screenStream = null;
let localstream = null;
let peer = null;
let peerRoom = null;
let hostPeerId = null;
let roomUserPeerId = [];
let roomConnections = {};
let localVCUser = {};
let remoteVCUsers = {};
let remoteCalls = {};
let connections = {};
let callHasEnd = null;
let connection = null;
let peerRoomId = "13e5bg19-82ac-43aq-a16b-5f0713";
let defaultsOpts = {
  audio: true,
  video: { facingMode: "user" },
};

let peerConfig = {
  debug: 2,
  config: {
    iceServers: [
      {urls: "stun:chirashi-kun.mirai-chi.com:80?transport=udp"},
      {
        urls: "turn:chirashi-kun.mirai-chi.com:443?transport=tcp",
        username: "legoodvn",
        credential: "tglsolutions11",
      },
    ],
  },
};

export default function VideoCallPage() {
  const router = useRouter();
  const {
    videoCallState: {
      endVideoCall,
      requestScreen,
      muteVideo,
      muteVideoCall,
      isShareScreen,
      crashPeerId,
      isDefaultCamera,
      mainStream,
    },
    videoCallDispatch,
  } = React.useContext<any>(VideoCallContext);
  const [userEndCall, setUserEndCall] = useState(false);
  callHasEnd = endVideoCall;
  llllll(53, defaultsOpts);

  function llllll(row, log) {
    console.log(row + "", log);
  }

  function processCrashedPeerId(index) {
    if (index > 3) return;
    llllll(46, "crashPeerId: " + crashPeerId);
    let peerCrashed = crashPeerId;
    videoCallDispatch({
      type: VideoCallAction.CRASH_PEERID,
      item: null,
    });

    let checkPeerAlive = new Peer(peerCrashed, peerConfig);
    checkPeerAlive.on("open", function (peerid) {
      llllll(55, "checkPeerAlive: " + false);
      deleteRemoteUser(peerCrashed);
      if (peerRoom) {
        Object.keys(roomConnections).forEach((item) => {
          sendMessageRoom(
            roomConnections[item],
            TypeMsg.PEER_LEAVE,
            peerCrashed
          );
        });
      } else {
        if (hostPeerId == peerCrashed) {
          createRoomPeer();
          setTimeout(() => {
            Object.keys(roomConnections).forEach((item) => {
              sendMessageRoom(
                roomConnections[item],
                TypeMsg.PEER_LEAVE,
                peerCrashed
              );
            });
          }, 5000);
        }
      }
      checkPeerAlive.disconnect();
      checkPeerAlive.destroy();
    });
    checkPeerAlive.on("error", function (err: ErrorEvent) {
      llllll(67, "checkPeerAlive: " + true);
      setTimeout(() => {
        processCrashedPeerId(index + 1);
      }, 2000);
    });
  }

  useEffect(() => {
    if (crashPeerId) {
      processCrashedPeerId(1);
    }
  }, [crashPeerId]);

  useEffect(() => {
    let vh = window.innerHeight;
    document.documentElement.style.setProperty("--vh", `${vh}px`);

    videoCallDispatch({
      type: VideoCallAction.DEVICE_DETECT,
      item: isTablet || isMobile,
    });
    let supports = navigator.mediaDevices.getSupportedConstraints();
    if (supports["facingMode"] === true) {
      videoCallDispatch({
        type: VideoCallAction.SUPPORT_FACEUSER,
        item: true,
      });
    }
  }, []);

  useEffect(() => {
    if (localstream) {
      localstream.getAudioTracks()[0].enabled = muteVideoCall;
    } 
  }, [muteVideoCall, localstream])
  
  useEffect(() => {
    if (localstream) {
      localstream.getVideoTracks()[0].enabled = muteVideo;
    } 
  },[muteVideo, localstream])

  useEffect(() => {
    if (endVideoCall) {
      llllll(79, "endVideoCall");

      videoCallDispatch({
        type: VideoCallAction.REMOTE_VCUSER,
        item: {},
      });
      videoCallDispatch({
        type: VideoCallAction.END_VIDEO_CALL,
        item: false,
      });
      const tracks = localstream.getTracks();
      tracks.forEach(function (track) {
        track.stop();
      });
      setUserEndCall(true);

      peer.destroy();
      peer = null;

      // setTimeout(() => {
      //   router.push("/").then(() => router.reload());
      // }, 1500);
    }
  }, [endVideoCall]);

  function replaceStream(mediaStream, includeAudio) {
    setTimeout(() => {
      llllll(101, "replaceStream");
      Object.keys(remoteCalls).forEach((item) => {
        let sender = remoteCalls[item].peerConnection.getSenders();
        let videoTrack = mediaStream.getVideoTracks()[0];
        var ssss = sender.find(function (s) {
          return s.track.kind == videoTrack.kind;
        });
        if (ssss) ssss.replaceTrack(videoTrack);

        if (includeAudio) {
          let audioTrack = mediaStream.getAudioTracks()[0];
          var aaaa = sender.find(function (s) {
            return s.track.kind == audioTrack.kind;
          });
          if (aaaa) aaaa.replaceTrack(audioTrack);
        }
      });
    }, 500);
  }

  useEffect(() => {
    if (requestScreen) {
      videoCallDispatch({
        type: VideoCallAction.REQUEST_SCREEN,
        item: false,
      });

      if (!isShareScreen) {
        llllll(120, "isShareScreen");
        const mediaDevices = navigator.mediaDevices as any;
        mediaDevices
          .getDisplayMedia({ video: true })
          .then((mediaStream) => {
            videoCallDispatch({
              type: VideoCallAction.LOCAL_VCUSER,
              item: {},
            });
            localVCUser[peer.id] = mediaStream;
            setTimeout(() => {
              videoCallDispatch({
                type: VideoCallAction.LOCAL_VCUSER,
                item: localVCUser,
              });
            }, 500);
            videoCallDispatch({
              type: VideoCallAction.IS_SHARE_SCREEN,
              item: true,
            });
            replaceStream(mediaStream, false);
            llllll(135, "shareScreen");

            Object.keys(roomConnections).forEach((item) => {
              sendMessageRoom(
                roomConnections[item],
                TypeMsg.SHARE_SCREEN,
                peer.id
              );
            });

            screenStream = mediaStream;
            mediaStream.getTracks()[0].onended = () => {
              llllll(139, "shareScreen onended");
              screenStream = null;
              videoCallDispatch({
                type: VideoCallAction.MAIN_STREAM,
                item: {},
              });
              videoCallDispatch({
                type: VideoCallAction.IS_SHARE_SCREEN,
                item: false,
              });
              llllll(149, "replaceStream localstream");
              replaceStream(localstream, false);
              videoCallDispatch({
                type: VideoCallAction.LOCAL_VCUSER,
                item: {},
              });
              localVCUser[peer.id] = localstream;
              setTimeout(() => {
                videoCallDispatch({
                  type: VideoCallAction.LOCAL_VCUSER,
                  item: localVCUser,
                });
              }, 500);
            };
          })
          .catch(() => {});
      } else {
        llllll(155, "stop screenStream");
        if (screenStream != null) {
          const tracks = screenStream.getTracks();
          tracks.forEach(function (track) {
            track.stop();
          });
        }
        screenStream = null;
        videoCallDispatch({
          type: VideoCallAction.MAIN_STREAM,
          item: {},
        });
        videoCallDispatch({
          type: VideoCallAction.IS_SHARE_SCREEN,
          item: false,
        });
        llllll(263, "replaceStream localstream");
        replaceStream(localstream, false);
        videoCallDispatch({
          type: VideoCallAction.LOCAL_VCUSER,
          item: {},
        });
        setTimeout(() => {
          localVCUser[peer.id] = localstream;
          videoCallDispatch({
            type: VideoCallAction.LOCAL_VCUSER,
            item: localVCUser,
          });
        }, 500);
      }
    }
  }, [requestScreen]);

  useEffect(() => {
    if (isDefaultCamera == null || localstream == null) return;
    localstream.getTracks().forEach((stream) => {
      stream.stop();
    });
    if (isDefaultCamera) defaultsOpts.video = { facingMode: "user" };
    else defaultsOpts.video = { facingMode: "environment" };
    navigator.getUserMedia =
      navigator.getUserMedia ||
      navigator.mediaDevices.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia;

    if (navigator.getUserMedia) {
      navigator.getUserMedia(
        defaultsOpts,
        function (stream) {
          llllll(218, "isDefaultCamera changed");
          videoCallDispatch({
            type: VideoCallAction.LOCAL_VCUSER,
            item: {},
          });
          localstream = stream;
          localVCUser[peer.id] = localstream;
          setTimeout(() => {
            videoCallDispatch({
              type: VideoCallAction.LOCAL_VCUSER,
              item: localVCUser,
            });
          }, 500);
          replaceStream(localstream, true);
        },
        function (err) {
          console.log("The following error occurred: " + err);
        }
      );
    } else {
      console.log("getUserMedia not supported");
    }
  }, [isDefaultCamera]);

  function processCameraStream() {
    navigator.getUserMedia =
      navigator.getUserMedia ||
      navigator.mediaDevices.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia;

    if (navigator.getUserMedia) {
      navigator.getUserMedia(
        defaultsOpts,
        function (stream) {
          llllll(188, "localstream");
          localstream = stream;
          peerRoomId = peerRoomId + router.query.roomid;
          createRoomPeer();
        },
        function (err) {
          console.log("The following error occurred: " + err.name);
        }
      );
    } else {
      console.log("getUserMedia not supported");
    }
  }
  


  useEffect(() => {
    if (router.query.roomid) {
      processCameraStream();
    }
  }, [router.query.roomid]);

  function sendMessageRoom(conn, type, item) {
    let message = {
      type: type,
      item: item,
    };
    conn.send(JSON.stringify(message));
  }

  function processMessageRoom(message) {
    let parseMessage = JSON.parse(message);
    if (parseMessage.type == TypeMsg.LIST_PEERID) {
      roomUserPeerId = parseMessage.item;
      roomUserPeerId.forEach((peerId) => {
        tryToMakeCall(peerId);
        // tryToMakeConnection(peerId);
      });
    }
    if (parseMessage.type == TypeMsg.PEER_LEAVE) {
      deleteRemoteUser(parseMessage.item);
    }
    if (parseMessage.type == TypeMsg.HOST_PEERID) {
      hostPeerId = parseMessage.item;
    }
    if (parseMessage.type == TypeMsg.ROOM_FULL) {
      // alert("MAX MEMBER IN ONE ROOM IS: " + Constant.ROOM_MAX_MEMBER);
      alert("このメーティングに参加できません。");
      router.push("/").then(() => router.reload());
    }
    if (parseMessage.type == TypeMsg.SHARE_SCREEN) {
      if (remoteVCUsers[parseMessage.item]) {
        let mainstream = {
          [Constant.PEER_MAIN]: remoteVCUsers[parseMessage.item],
          peerId: parseMessage.item,
        };
        videoCallDispatch({
          type: VideoCallAction.MAIN_STREAM,
          item: mainstream,
        });
      }
    }
  }

  function createRoomPeer() {
    llllll(233, "createRoomPeer: " + peerRoomId);
    peerRoom = new Peer(peerRoomId, peerConfig);
    peerRoom.on("open", function (peerid) {
      llllll(236, "createRoomPeer success: " + peerRoomId);
      if (peer == null) createNewPeer();
      hostPeerId = peer.id;
      peerRoom.on("connection", function (conn) {
        llllll(240, "peerRoom connection from: " + conn.peer);
        roomConnections[conn.peer] = conn;
        roomConnections[conn.peer].on("open", function () {
          llllll(243, "roomConnections[conn.peer] open: " + conn.peer);
          if (roomUserPeerId.length >= Constant.ROOM_MAX_MEMBER) {
            sendMessageRoom(
              roomConnections[conn.peer],
              TypeMsg.ROOM_FULL,
              Constant.ROOM_MAX_MEMBER
            );
            return;
          }

          let peerUser = roomUserPeerId.find((item) => item == conn.peer);
          if (peerUser == null) roomUserPeerId.push(conn.peer);

          sendMessageRoom(
            roomConnections[conn.peer],
            TypeMsg.LIST_PEERID,
            roomUserPeerId
          );
          if (peer?.id)
            sendMessageRoom(
              roomConnections[conn.peer],
              TypeMsg.HOST_PEERID,
              peer.id
            );
          else
            setTimeout(() => {
              if (peer?.id)
                sendMessageRoom(
                  roomConnections[conn.peer],
                  TypeMsg.HOST_PEERID,
                  peer.id
                );
            }, 2000);
        });

        roomConnections[conn.peer].on("close", function () {
          llllll(279, "roomConnections[conn.peer] close: " + conn.peer);
          deleteRemoteUser(conn.peer);
          Object.keys(roomConnections).forEach((item) => {
            sendMessageRoom(
              roomConnections[item],
              TypeMsg.PEER_LEAVE,
              conn.peer
            );
          });
        });

        roomConnections[conn.peer].on("error", function () {
          llllll(291, "roomConnections[conn.peer] error: " + conn.peer);
          deleteRemoteUser(conn.peer);
          Object.keys(roomConnections).forEach((item) => {
            sendMessageRoom(
              roomConnections[item],
              TypeMsg.PEER_LEAVE,
              conn.peer
            );
          });
        });

        roomConnections[conn.peer].on("disconnected", function () {
          llllll(303, "roomConnections[conn.peer] disconnected: " + conn.peer);
          deleteRemoteUser(conn.peer);
          Object.keys(roomConnections).forEach((item) => {
            sendMessageRoom(
              roomConnections[item],
              TypeMsg.PEER_LEAVE,
              conn.peer
            );
          });
        });
      });
    });

    peerRoom.on("error", function (err: ErrorEvent) {
      llllll(317, "peerRoom error: " + err);
      peerRoom = null;
      if (peer == null) createNewPeer();
      else connectRoomPeer();
    });
  }

  function createNewPeer() {
    llllll(325, "createNewPeer");
    if (peer != null) return;
    peer = new Peer(peerConfig);
    peer.on("open", function (peerid) {
      llllll(329, "createNewPeer success: " + peerid);
      localVCUser[peerid] = localstream;
      videoCallDispatch({
        type: VideoCallAction.LOCAL_VCUSER,
        item: localVCUser,
      });

      peer.on("call", function (call) {
        llllll(337, "call from: " + call.peer);
        if (screenStream != null) {
          screenStream.addTrack(localstream.getAudioTracks()[0]);
          call.answer(screenStream);
        } else {
          call.answer(localVCUser[Object.keys(localVCUser)[0]]);
        }
        call.on("stream", function (stream) {
          llllll(345, "call stream");
          remoteVCUsers[call.peer] = stream;
          remoteCalls[call.peer] = call;
          videoCallDispatch({
            type: VideoCallAction.REMOTE_VCUSER,
            item: remoteVCUsers,
          });
          videoCallDispatch({
            type: VideoCallAction.ADDED_VCUSER,
            item: call.peer,
          });
        });
      });

      connectRoomPeer();
    });
    peer.on("error", function (err: ErrorEvent) {
      llllll(362, "peer error" + err);
      if (Object.keys(localVCUser).length <= 0) createNewPeer();
    });
  }

  function connectRoomPeer() {
    llllll(368, "connectRoomPeer");
    connection = peer.connect(peerRoomId);
    connection.on("open", function () {
      llllll(371, "connectRoomPeer success: " + peerRoomId);
      connection.on("data", function (data) {
        llllll(373, "data received: " + data);
        processMessageRoom(data);
      });
    });

    connection.on("error", function (err) {
      llllll(379, "connectRoomPeer error: " + err);
    });

    connection.on("close", function () {
      llllll(383, "connectRoomPeer close");
      if (hostPeerId) deleteRemoteUser(hostPeerId);
      if (!callHasEnd) createRoomPeer();
    });
  }

  function tryToMakeCall(peerId) {
    if (peer.id == peerId) return;
    llllll(391, "tryToMakeCall from " + peer.id + " to " + peerId);

    remoteCalls[peerId] = peer.call(
      peerId,
      localVCUser[Object.keys(localVCUser)[0]]
    );
    remoteCalls[peerId].on("stream", function (stream) {
      llllll(398, "remoteCalls stream");
      remoteVCUsers[peerId] = stream;
      videoCallDispatch({
        type: VideoCallAction.REMOTE_VCUSER,
        item: remoteVCUsers,
      });
      videoCallDispatch({
        type: VideoCallAction.ADDED_VCUSER,
        item: peerId,
      });
    });

    remoteCalls[peerId].on("disconnected", () => {
      llllll(411, "remoteCalls[peerId] disconnected" + peerId);
    });

    remoteCalls[peerId].on("close", () => {
      // deleteRemoteUser(peerId);
      llllll(416, "remoteCalls[peerId] close" + peerId);
    });

    remoteCalls[peerId].on("error", (error) => {
      // deleteRemoteUser(peerId);
      llllll(421, "remoteCalls[peerId] error " + peerId) + " : " + error;
    });
  }

  // function tryToMakeConnection(peerId) {
  //   if (peer.id == peerId) return;
  // console.log("tryToMakeConnection: " + peer.id + " -- " + peerId);
  // connections[peerId] = peer.connect(peerId);
  // connections[peerId].on("open", function () {});
  // connections[peerId].on("close", function () {
  //   deleteRemoteUser(peerId);
  // });
  // }

  function deleteRemoteUser(peerId) {
    llllll(436, "deleteRemoteUser" + peerId);
    let index = roomUserPeerId.indexOf(peerId);
    if (index >= 0) roomUserPeerId.splice(index, 1);
    delete roomConnections[peerId];
    delete remoteVCUsers[peerId];
    delete remoteCalls[peerId];
    delete connections[peerId];
    videoCallDispatch({
      type: VideoCallAction.REMOTE_VCUSER,
      item: remoteVCUsers,
    });
    videoCallDispatch({
      type: VideoCallAction.REMOVE_VCUSER,
      item: peerId,
    });
    if (Object.keys(mainStream).length > 0) {
      if (mainStream.peerId == peerId) {
        videoCallDispatch({
          type: VideoCallAction.MAIN_STREAM,
          item: {},
        });
      }
    }
  }

  return (
    <VideoCallWrapper>
      {userEndCall == false && (
        <>
          <MUnOwnerVideo />
          <ControlBarTop />
          <ControlBarBottom />
          <OwnerVideo />
          {/* <ControlBarRight /> */}
        </>
      )}
      {userEndCall == true && (
        <div className="div-end-call">通話は終了しました。ありがとうございました！</div>
      )}
    </VideoCallWrapper>
  );
}
