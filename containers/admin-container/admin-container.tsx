import { Layout, Menu } from 'antd';
import { UserOutlined, VideoCameraOutlined } from '@ant-design/icons';
import styled from 'styled-components';
import { useState } from 'react';
import AdminHome from 'containers/admin-home/admin-home';
import AdminCalendar from 'containers/admin-calendar/admin-calendar';
import { useAuthAdmin } from 'hooks/use-auth-admin';
import { useRouter } from 'next/router';
const { Header, Content, Footer, Sider } = Layout;

type props = {};
export default function AdminContainer({}: props) {
  const router = useRouter();
  const { signOut } = useAuthAdmin();
  const [selectedKey, setSelectedKey] = useState('1');

  return (
    <Wrapper>
      <Layout style={{ minHeight: '100vh' }}>
        <Sider
          breakpoint='lg'
          collapsedWidth='0'
          onBreakpoint={(broken) => {
            console.log('sidebar breakpoint: ', broken);
          }}
          onCollapse={(collapsed, type) => {
            console.log(collapsed, type);
          }}>
          <div className='logo'>予約管理システム</div>
          <Menu theme='dark' mode='inline' defaultSelectedKeys={['1']}>
            <Menu.Item key='1' icon={<UserOutlined />} onClick={() => setSelectedKey('1')}>
              ホーム
            </Menu.Item>
            <Menu.Item key='2' icon={<VideoCameraOutlined />} onClick={() => setSelectedKey('2')}>
              設定
            </Menu.Item>
            <Menu.Item key='3' icon={<UserOutlined />} onClick={signOut}>
              ログアウト
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header className='site-layout-sub-header-background' style={{ padding: 0 }} />
          <Content
            style={{
              margin: '24px 16px 0',
              height: 'calc(100vh - 136px)',
              overflow: 'scroll',
            }}>
            <div className='site-layout-background' style={{ padding: 24, minHeight: 360 }}>
              {selectedKey === '1' && (
                <AdminHome
                  name={router.pathname === '/aquarium/admin' ? 'しながわ水族館' : 'オバケン'}
                />
              )}
              {selectedKey === '2' && <AdminCalendar />}
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}></Footer>
        </Layout>
      </Layout>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  min-height: 100vh;
  width: 100vw;
  .logo {
    color: white;
    height: 64px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .site-layout-background {
    height: 100% !important;
  }
`;
