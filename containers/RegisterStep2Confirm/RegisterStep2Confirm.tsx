import { Row, Col, Typography, Form, Input, Radio, Divider, List, Card } from 'antd';
import { RegisterFormData } from 'interfaces';
import { CSSProperties } from 'react';
import styled from 'styled-components';
import moment from 'moment';

const { Paragraph, Title } = Typography;

type props = {
  style?: CSSProperties;
  data: RegisterFormData;
};
export default function RegisterStep2Confirm({ style, data }: props) {
  return (
    <Wrapper style={style}>
      <Row justify='center'>
        <Col span={24}>
          <Title level={3} style={{ textAlign: 'center' }}>
            予約内容をご確認の上、「予約する」ボタンを押してください。
          </Title>
        </Col>
        <Col span={24} md={16} className='border-round'>
          <Row>
            <Col span={6}>
              <Label>{labelData[0]}</Label>
            </Col>
            <Col span={18}>
              <Para>{`${moment(data.time).format('YYYY年MM月DD日 HH:mm')} ~ ${moment(data.time)
                .add(30, 'minutes')
                .format('HH:mm')}`}</Para>
            </Col>
          </Row>
          <Divider />
          <Row>
            <Col span={6}>
              <Label>{labelData[1]}</Label>
            </Col>
            <Col span={18}>
              <Para>{data.type === 'first_time' ? '初参加' : '二回目以上'}</Para>
            </Col>
          </Row>
          <Divider />
          <Row>
            <Col span={6}>
              <Label>{labelData[2]}</Label>
            </Col>
            <Col span={18}>
              <Para>
                {data.name.first}
                {data.name.last}
              </Para>
            </Col>
          </Row>
          <Divider />
          <Row>
            <Col span={6}>
              <Label>{labelData[4]}</Label>
            </Col>
            <Col span={18}>
              <Para>{data.tel}</Para>
            </Col>
          </Row>
          <Divider />
          <Row>
            <Col span={6}>
              <Label>{labelData[5]}</Label>
            </Col>
            <Col span={18}>
              <Para>{data.address}</Para>
            </Col>
          </Row>
          <Divider />
          <Row>
            <Col span={6}>
              <Label>{labelData[6]}</Label>
            </Col>
            <Col span={18}>
              <Para style={{ whiteSpace: 'pre-wrap' } as CSSProperties}>{data.content}</Para>
            </Col>
          </Row>
        </Col>
      </Row>
    </Wrapper>
  );
}

const labelData = ['日時', 'タイプ', 'お名前', '電話番号', 'メールアドレス', '備考'];
const Wrapper = styled.div`
  color: white;
  .ant-divider {
    background-color: white;
    margin-top: 12px;
    margin-bottom: 12px;
  }
  .border-round {
    border: 1px solid white;
    border-radius: 8px;
    padding: 1.5rem 2rem;
  }
`;

const Para = styled(Paragraph)`
  /* font-size: 20px; */
  font-size: 1.1rem;
  margin-bottom: 0 !important;
`;
const Label = styled(Paragraph)`
  /* font-size: 20px; */
  font-size: 1.1rem;
  font-weight: 700;
  margin-bottom: 0 !important;
`;
