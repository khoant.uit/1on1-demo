import { Row, Col, Typography, Form, Input, Radio } from 'antd';
import moment from 'moment';
import { useEffect } from 'react';
import { CSSProperties } from 'react';
import styled from 'styled-components';

const { Paragraph, Title } = Typography;
type props = {
  style?: CSSProperties;
  initialValues: {
    time: Date;
  };
  form: any;
  onFinish: (value: any) => void;
};
export default function RegisterStep2({ initialValues, style, form, onFinish }: props) {
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  useEffect(() => {
    form.setFieldsValue(initialValues);
  }, [initialValues]);

  return (
    <Wrapper style={style}>
      <Row justify='center'>
        <Col span={24} md={16}>
          <Title level={3} style={{ textAlign: 'center' }}>
            お客様情報を入力してください
          </Title>
          <Form
            form={form}
            preserve
            labelCol={{ span: 4 }}
            labelAlign='left'
            wrapperCol={{ span: 20 }}
            name='basic'
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}>
            <Form.Item label='日時' hidden>
              <Form.Item name='time' noStyle>
                <Input hidden />
              </Form.Item>
              <Input
                disabled
                value={`${moment(initialValues.time).format('YYYY年MM月DD日 HH:mm')} ~ ${moment(
                  initialValues.time
                )
                  .add(30, 'minutes')
                  .format('HH:mm')}`}
              />
            </Form.Item>

            <Form.Item
              name='type'
              label='タイプ'
              rules={[
                {
                  required: true,
                  message: 'これは必須な項目です。',
                },
              ]}>
              <Radio.Group>
                <Radio value='first_time'>初参加</Radio>
                <Radio value='more_time'>二回目以上</Radio>
              </Radio.Group>
            </Form.Item>

            <Form.Item
              label={
                <label>
                  <span style={{ color: 'red' }}>*</span> お名前
                </label>
              }
              name='name'
              // rules={[
              //   {
              //     required: true,
              //     message: 'これは必須な項目です。',
              //   },
              // ]}
            >
              <Input.Group compact>
                <Form.Item
                  name={['name', 'first']}
                  noStyle
                  rules={[
                    {
                      required: true,
                      message: 'これは必須な項目です。',
                    },
                  ]}>
                  <Input placeholder='麻生' style={{ width: '49%' }} />
                </Form.Item>
                <Form.Item
                  name={['name', 'last']}
                  noStyle
                  rules={[
                    {
                      required: true,
                      message: 'これは必須な項目です。',
                    },
                  ]}>
                  <Input placeholder='太郎' style={{ width: '49%', marginLeft: '2%' }} />
                </Form.Item>
              </Input.Group>
            </Form.Item>

            <Form.Item
              label='電話番号'
              name='tel'
              rules={[
                {
                  required: true,
                  message: 'これは必須な項目です。',
                },
              ]}>
              <Input placeholder='080-1234-5678' />
            </Form.Item>

            <Form.Item
              label='メールアドレス'
              name='address'
              rules={[
                {
                  required: true,
                  message: 'これは必須な項目です。',
                },
              ]}>
              <Input />
            </Form.Item>

            <Form.Item label='備考' name='content'>
              <Input.TextArea rows={4} />
            </Form.Item>
          </Form>
          <Paragraph style={{ textAlign: 'center' }}>*は入力必須項目 (required)</Paragraph>
          <Paragraph style={{ textAlign: 'center' }}>
            ※トライアル時にはここにツアー内容を記載します。
          </Paragraph>
        </Col>
      </Row>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  .ant-form label {
    color: white !important;
    font-weight: 700;
  }
  input {
    height: 30px;
  }
  label {
    /* font-size: 15px; */
  }
  input,
  textarea {
    /* font-size: 15px; */
    border-radius: 5px !important;
    color: white !important;
    background-color: transparent !important;
    border-color: white;
  }
  .ant-radio-group {
    width: 100%;
    display: flex;
    justify-content: space-evenly;
  }
`;
