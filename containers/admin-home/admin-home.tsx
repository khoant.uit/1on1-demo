import styled from 'styled-components';
import { Row, Col, Typography } from 'antd';
import React from 'react';
import { useAuthAdmin } from 'hooks/use-auth-admin';

const { Title } = Typography;
type props = {
  name: string;
};
export default function AdminHome({ name }: props) {
  const { signOut } = useAuthAdmin();

  return (
    <Wrapper>
      <Row align='middle' justify='center' style={{ height: '100%' }}>
        <Col>
          <Title level={3}>WEBツアー予約管理システム</Title>
          {/* <Title level={4}>{name}</Title> */}
          <button onClick={signOut}>ログアウト</button>
        </Col>
      </Row>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  h3,
  h4 {
    color: black !important;
  }
  h4 {
    margin-top: 0.8rem !important;
  }
  display: block;
  font-size: 20px;
  text-align: center;
  height: 100%;
  button {
    background-color: #1890ff;
    color: white;
    border-width: 0;
    cursor: pointer;
    padding: 0.3rem 1.2rem;
    font-size: 1.2rem;
  }
`;
