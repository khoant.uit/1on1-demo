import styled from 'styled-components';
import { Row, Col, Typography } from 'antd';
import BoxButton from 'components/box-button/box-button';
import { useRouter } from 'next/router';

type props = {
  title: string;
  onClickBook: (e) => void;
  onClickVideo: (e) => void;
  onClickMap?: (e) => void;
};
export default function RegisterLangding({ onClickBook, title, onClickVideo, onClickMap }: props) {
  const router = useRouter();
  const { pathname } = router;
  console.log(pathname);

  return (
    <Wrapper>
      <Row justify='center'>
        <Col span={22} style={{ display: 'flex', alignItems: 'center', height: '100vh' }}>
          <div>
            {/* <LargeTitle>Get close to</LargeTitle>
            <LargeTitle>nature</LargeTitle> */}
            <LargeTitle>{title}</LargeTitle>
            {/* <SmallText>Experuebce the ocean in your own home</SmallText>
            <SmallText>
              Experuebce the ocean in your own home Experuebce the ocean in your own home
            </SmallText> */}
            <SmallText>自宅でおすすめ！</SmallText>
            <SmallText>オンライン体験ツアー</SmallText>
          </div>
          <FooterText>
            <SmallText></SmallText>
            <BoxSession>
              <BoxButton onClick={onClickBook} text='事前予約' subText='' isColor />
              {router.pathname === '/aquarium/register' && (
                <BoxButton onClick={onClickMap} text='地図' subText='' />
              )}
              <BoxButton onClick={onClickVideo} text='動画' subText='' />
            </BoxSession>
          </FooterText>
        </Col>
      </Row>
    </Wrapper>
  );
}
const { Text, Title, Paragraph } = Typography;
const Wrapper = styled.div``;
const LargeTitle = styled(Paragraph)`
  /* font-size: 100px; */
  font-size: 6rem;
  color: white;
  line-height: 115px;
  margin-bottom: 0px !important;
`;
const SmallText = styled(Paragraph)`
  /* font-size: 30px; */
  font-size: 1.7rem;
  color: white;
  margin-bottom: 0px !important;
`;
const FooterText = styled.div`
  position: fixed;
  bottom: 0;
  display: flex;
  align-items: flex-end;
`;
const BoxSession = styled.div`
  position: fixed;
  bottom: 0;
  right: 0;
  display: flex;
`;
