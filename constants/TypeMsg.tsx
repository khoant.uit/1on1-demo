export default class TypeMsg {
    static LIST_PEERID = 111;
    static CHAT_MSG = 222;
    static PEER_LEAVE = 333;
    static HOST_PEERID = 444;
    static ROOM_FULL = 555;
    static SHARE_SCREEN = 666;
}