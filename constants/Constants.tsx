export default class Constant {
  static PEER_MAIN = 'mainstream';
  static ROOM_MAX_MEMBER = 3;
}
export const keyTime = 'YYMMDDHHmm';
