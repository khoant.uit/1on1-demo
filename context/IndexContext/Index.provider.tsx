import React, { useReducer } from "react";
import { IndexContext } from "./Index.context";

const INITIAL_STATE = {};

function reducer(state: any, action: any) {
  state = INITIAL_STATE;
  switch (action.type) {
    // case GCActions.OTHER_STREAM:
    //   state.otherStreamId = action.item;
    //   return {
    //     ...state,
    //   };
    default:
      return state;
  }
}

export const IndexProvider: React.FunctionComponent = ({ children }) => {
  const [indexState, indexDispatch] = useReducer(
    reducer,
    INITIAL_STATE
  );
  return (
    <IndexContext.Provider
      value={{ indexState, indexDispatch }}
    >
      {children}
    </IndexContext.Provider>
  );
};
