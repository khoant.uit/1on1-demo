export default class VideoCallAction {
    static LOCAL_VCUSER = 100;
    static REMOTE_VCUSER = 101;
    static END_VIDEO_CALL = 102;
    static ADDED_VCUSER = 103;
    static REMOVE_VCUSER = 104;

    static REQUEST_SCREEN = 105;
    static MAIN_STREAM = 106;
    static IS_SHARE_SCREEN = 107;
    static CRASH_PEERID = 108;
    static DEVICE_DETECT = 109;
    static MUTE_CALL = 110;
    static MUTE_CALL_VIDEO = 111;
    
    static DEFAULT_CAMERA = 201;
    static SUPPORT_FACEUSER = 202;
}
