import React, { useReducer } from "react";
import { VideoCallContext } from "./VideoCall.context";
import VideoCallAction from "./VideoCallAction";

const INITIAL_STATE = {
  localVCUser: {},
  remoteVCUsers: {},
  endVideoCall: false,
  muteVideoCall: false,
  muteVideo: false,
  addedVCUser: null,
  removeVCUser: null,
  requestScreen: false,
  isShareScreen: false,
  mainStream: {},
  crashPeerId: null,
  isMobileOrTablet: false,
  isDefaultCamera: null,
  supportFaceUser: false,
};

function reducer(state: any, action: any) {
  state = INITIAL_STATE;
  switch (action.type) {
    case VideoCallAction.LOCAL_VCUSER:
      state.localVCUser = action.item;
      return {
        ...state,
      };
    case VideoCallAction.REMOTE_VCUSER:
      state.remoteVCUsers = action.item;
      return {
        ...state,
      };
    case VideoCallAction.MUTE_CALL:
      state.muteVideoCall = action.item;
      return {
        ...state,
      };
    case VideoCallAction.MUTE_CALL_VIDEO:
      state.muteVideo = action.item;
      return {
        ...state,
      };
    case VideoCallAction.END_VIDEO_CALL:
      state.endVideoCall = action.item;
      return {
        ...state,
      };
    case VideoCallAction.ADDED_VCUSER:
      state.addedVCUser = action.item;
      return {
        ...state,
      };
    case VideoCallAction.REMOVE_VCUSER:
      state.removeVCUser = action.item;
      return {
        ...state,
      };
    case VideoCallAction.REQUEST_SCREEN:
      state.requestScreen = action.item;
      return {
        ...state,
      };
    case VideoCallAction.IS_SHARE_SCREEN:
      state.isShareScreen = action.item;
      return {
        ...state,
      };
    case VideoCallAction.MAIN_STREAM:
      state.mainStream = action.item;
      return {
        ...state,
      };
    case VideoCallAction.CRASH_PEERID:
      state.crashPeerId = action.item;
      return {
        ...state,
      };
    case VideoCallAction.DEVICE_DETECT:
      state.isMobileOrTablet = action.item;
      return {
        ...state,
      };
    case VideoCallAction.DEFAULT_CAMERA:
      state.isDefaultCamera = action.item;
      return {
        ...state,
      };
    case VideoCallAction.SUPPORT_FACEUSER:
      state.supportFaceUser = action.item;
      return {
        ...state,
      };
    default:
      return state;
  }
}

export const VideoCallProvider: React.FunctionComponent = ({ children }) => {
  const [videoCallState, videoCallDispatch] = useReducer(
    reducer,
    INITIAL_STATE
  );
  return (
    <VideoCallContext.Provider value={{ videoCallState, videoCallDispatch }}>
      {children}
    </VideoCallContext.Provider>
  );
};
