import { BookingInfo } from 'interfaces';
import { createContext, Dispatch, SetStateAction, useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { message } from 'antd';

const BookingInfoContext = createContext<[BookingInfo, Dispatch<SetStateAction<BookingInfo>>]>([
  {},
  () => {},
]);
const BookingInfoProvider = (props) => {
  const router = useRouter();
  const pjName = router.pathname.includes('aquarium')
    ? 'aquarium'
    : router.pathname.includes('ghost')
    ? 'ghost'
    : 'ghost';
  // if (pjName === 'aaa') {
  //   throw 'errr in context';
  // }

  const [state, setState] = useState<BookingInfo>({});
  useEffect(() => {
    setState(JSON.parse(localStorage.getItem('bookingInfo' + pjName)) ?? {});
    window.addEventListener('storage', () => {
      // console.log('storage change');
      if (router.pathname.includes('/admin')) {
        message.info('新規予約を受け取りました。');
      }
      setState(JSON.parse(localStorage.getItem('bookingInfo' + pjName)) ?? {});
    });
  }, []);
  useEffect(() => {
    console.log('booking state change: ', state);
    localStorage.setItem('bookingInfo' + pjName, JSON.stringify(state));
  }, [state]);
  return (
    <BookingInfoContext.Provider value={[state, setState]}>
      {props.children}
    </BookingInfoContext.Provider>
  );
};

export { BookingInfoContext, BookingInfoProvider };
