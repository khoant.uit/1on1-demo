import { AuthAdmin } from 'interfaces';
import { useEffect } from 'react';
import { createContext, Dispatch, SetStateAction, useState } from 'react';

const AuthAdminContext = createContext<[AuthAdmin, Dispatch<SetStateAction<AuthAdmin>>]>([
  {
    isAuthenticated: false,
    name: '',
  },
  () => {},
]);

const AuthAdminProvider = (props) => {
  const [state, setState] = useState<AuthAdmin>({
    isAuthenticated: false,
    name: '',
  });
  useEffect(() => {
    console.log('admin state change: ', state.isAuthenticated);
  }, [state]);

  return (
    <AuthAdminContext.Provider value={[state, setState]}>
      {props.children}
    </AuthAdminContext.Provider>
  );
};

export { AuthAdminContext, AuthAdminProvider };
