import { keyTime } from 'constants/Constants';
import { RegisterFormData } from 'interfaces';
import moment from 'moment';

export default function handler(req, res) {
  const formData = req.body as RegisterFormData;
  let nodemailer = require('nodemailer');
  const transporter = nodemailer.createTransport({
    service: '465',
    host: 'smtp.gmail.com',
    auth: {
      user: 'tglsol06092019@gmail.com',
      pass: process.env.EMAIL_PASS,
    },
    secure: true,
  });

  console.log('api: run into send mail fuction');
  console.log('api: curvar is: ' + process.env.EMAIL_PASS);

  const roomId = moment(formData.time).format(keyTime);
  // const url = window.location.origin + '/videocall?roomid=' + roomId;
  const url = '1on1-demo.vercel.app' + '/videocall?roomid=' + roomId;

  var mailOptions = {
    from: 'khoant.uit@gmail.com',
    to: formData.address,
    subject: 'Booking detail!',
    text: 'That was easy!',
    html: `<div>
    <p>Thank you for using our booking service, ${formData.name.first}${formData.name.last}.</p>
    <p>This is a link to your booking session: <a href='${url}'>${url}</a></p>
    <p>Please join in time.</p>
  </div>`,
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
    }
    console.log('Email sent: ' + info.response);
  });

  res.status(200).json({ name: 'Jo' });
}
