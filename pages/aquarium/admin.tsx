import LoginForm from 'containers/LoginForm/login-form';
import AdminContainer from 'containers/admin-container/admin-container';
import { Wrapper } from 'styles/pages.styled';
import { useAuthAdmin } from 'hooks/use-auth-admin';

export default function AdminPage() {
  const { isAuthenticated } = useAuthAdmin();

  return <Wrapper>{!isAuthenticated ? <LoginForm title='水族館' /> : <AdminContainer />}</Wrapper>;
}
