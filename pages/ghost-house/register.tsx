import { Wrapper } from 'styles/pages.styled';
import { Typography, Steps, Row, Col, Form } from 'antd';
import React, { CSSProperties, useEffect } from 'react';
import RegisterStep1 from 'containers/RegisterStep1/RegisterStep1';
import RegisterStep2 from 'containers/RegisterStep2/RegisterStep2';
import RegisterStep3 from 'containers/RegisterStep3/RegisterStep3';
import RegisterStep2Confirm from 'containers/RegisterStep2Confirm/RegisterStep2Confirm';
import { useState } from 'react';
import moment from 'moment';
import { RegisterFormData } from 'interfaces';
import RegisterLangding from 'containers/register-landing/register-landing';
import styled from 'styled-components';
import BoxButton from 'components/box-button/box-button';
import { useBookingInfo } from 'hooks/use-booking-info';
import { keyTime } from 'constants/Constants';

const { Title } = Typography;
const { Step } = Steps;
export default function RegisterPage() {
  const { addNewBooking, bookingInfo } = useBookingInfo();
  const [form] = Form.useForm();
  const [current, setCurrent] = useState(0);
  const [playFullVid, setPlayFulVid] = useState(false);
  const [renderLanding, setRenderLanding] = useState(true);
  const [step1Value, setStep1Value] = useState<Date>(moment().hour(10).startOf('hour').toDate());
  const [step2Value, setStep2Value] = useState<RegisterFormData>({
    time: '',
    type: 'first_time',
    name: { first: '', last: '' },
    tel: '',
    address: '',
    content: '',
  });
  const [isStep2Confirmed, setIiStep2Confirmed] = useState(false);
  useEffect(() => {
    for (let i = 10; i < 18; i++) {
      const key = moment().hour(i).startOf('hour').format(keyTime);
      const key30 = moment().hour(i).minute(30).startOf('hour').format(keyTime);

      if (bookingInfo.hasOwnProperty(key)) {
        setStep1Value(moment().hour(i).startOf('hour').toDate());
        break;
      }
      if (bookingInfo.hasOwnProperty(key30)) {
        setStep1Value(moment().hour(i).minute(30).startOf('minute').toDate());
        break;
      }
    }
  }, []);

  const onFinishForm = (value) => {
    setStep2Value(value);
    next();
  };
  const steps = [
    {
      title: '日時選択',
      content: <RegisterStep1 value={step1Value} onChange={setStep1Value} />,
    },
    {
      title: 'お客様情報の入力',
      content: (
        <RegisterStep2 onFinish={onFinishForm} form={form} initialValues={{ time: step1Value }} />
      ),
      contentConfirm: <RegisterStep2Confirm data={step2Value} />,
    },
    {
      title: '予約完了',
      content: <RegisterStep3 data={step2Value} />,
    },
  ];

  const handleProcessToNextStep = () => {
    if (current === 1 && isStep2Confirmed === false) {
      form.submit();
      return;
    }
    if (current === 1 && isStep2Confirmed) {
      console.log('add new booking: ' + JSON.stringify(step2Value));
      addNewBooking(step2Value);
    }
    next();
  };

  const next = () => {
    if (current === 1 && isStep2Confirmed === false) {
      setIiStep2Confirmed(true);
      return;
    }
    setCurrent(current + 1);
  };

  const prev = () => {
    if (current === 1 && isStep2Confirmed === true) {
      setIiStep2Confirmed(false);
      return;
    }
    setCurrent(current - 1);
  };

  return (
    <>
      {playFullVid && false && (
        <FooterText>
          <BoxSession>
            <BoxButton
              onClick={() => setRenderLanding(false)}
              text='Book in advance'
              subText='content content'
              isColor
            />
            <BoxButton
              onClick={() => setPlayFulVid(!playFullVid)}
              text='Video'
              subText='content content'
            />
          </BoxSession>
        </FooterText>
      )}
      {playFullVid && (
        <>
          <iframe
            style={{ position: 'absolute', left: 0, right: 0 }}
            // width='560'
            // height='315'
            width='100%'
            height='100%'
            src='https://www.youtube.com/embed/A6aRkhlqWuE'
            title='YouTube video player'
            frameBorder='0'
            allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
            allowFullScreen></iframe>
          {/* <iframe
            // width='560'
            // height='315'
            width='100%'
            height='100%'
            src='https://www.youtube.com/watch?v=A6aRkhlqWuE'
            title='YouTube video player'
            frameBorder='0'
            allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
            allowFullScreen
          /> */}
        </>
      )}
      {!playFullVid && (
        <div style={{ width: '100vw', height: '100vh', position: 'fixed', background: 'black' }}>
          <video
            muted
            style={{ position: 'fixed', right: 0, bottom: 0, width: '100%', height: '100%' }}
            id='bg-video'
            src='/bg-video-ghost-long.mp4'
            autoPlay
            loop
          />
        </div>
      )}
      <Wrapper
      // style={{ background: "url('/bg-ghost.png')", backgroundSize: 'contain' } as CSSProperties}
      >
        {renderLanding
          ? !playFullVid && (
              <RegisterLangding
                title='オバケンWEBツアー'
                onClickBook={() => setRenderLanding(false)}
                onClickVideo={() => setPlayFulVid(!playFullVid)}
              />
            )
          : !playFullVid && (
              <Row key={`${renderLanding}`} justify='center'>
                <Col span={22} md={20} style={{ paddingTop: '4vh' }}>
                  <Title style={{ textAlign: 'center' }}>オバケンWEBツアー</Title>
                  <Steps current={current}>
                    {steps.map((item) => (
                      <Step key={item.title} title={item.title} />
                    ))}
                  </Steps>
                  <div className='black-session'>
                    <div className='steps-content'>
                      {steps.map((step, index) =>
                        index !== 1 ? (
                          React.cloneElement(step.content, {
                            key: index,
                            style: { display: index === current ? 'block' : 'none' },
                          })
                        ) : (
                          <React.Fragment key={index}>
                            {React.cloneElement(step.content, {
                              style: {
                                display: index === current && !isStep2Confirmed ? 'block' : 'none',
                              },
                            })}
                            {React.cloneElement(step.contentConfirm, {
                              style: {
                                display: index === current && isStep2Confirmed ? 'block' : 'none',
                              },
                            })}
                          </React.Fragment>
                        )
                      )}
                    </div>
                    <div className='steps-action'>
                      {current > 0 && current !== steps.length - 1 && (
                        <ProcessButton style={{ margin: '0 8px' }} onClick={() => prev()}>
                          戻る
                        </ProcessButton>
                      )}
                      {current < steps.length - 1 && (
                        <ProcessButton
                          style={{ background: '#FA8DFA' }}
                          onClick={() => handleProcessToNextStep()}>
                          {current === 1 && isStep2Confirmed === true ? '予約する' : '次へ'}
                        </ProcessButton>
                      )}
                      {current === steps.length - 1 && (
                        <ProcessButton
                          style={{ marginLeft: 'auto', marginRight: 'auto' }}
                          onClick={() => {
                            setRenderLanding(true);
                            setIiStep2Confirmed(false);
                            setCurrent(0);
                          }}>
                          完了
                        </ProcessButton>
                      )}
                    </div>
                  </div>
                </Col>
              </Row>
            )}
      </Wrapper>
    </>
  );
}

const ProcessButton = styled.button`
  width: 135px;
  height: 41px;
  border-radius: 5px;
  background-color: #5a76fc;
  color: white;
  border-width: 0;
  cursor: pointer;
`;
const FooterText = styled.div`
  position: fixed;
  bottom: 0;
  right: 0;
  z-index: 10;
`;
const BoxSession = styled.div`
  position: fixed;
  bottom: 0;
  right: 0;
  display: flex;
`;
