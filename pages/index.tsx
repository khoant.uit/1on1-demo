import Head from "next/head";
import IndexPage from "../containers/IndexPage/IndexPage";
import { IndexProvider } from "../context/IndexContext/Index.provider";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <IndexProvider>
       <IndexPage/>
    </IndexProvider>
  );
}
