import React from "react";
import { VideoCallProvider } from "../context/VideoCallContext/VideoCall.provider";
import dynamic from 'next/dynamic';
const VideoCallPage = dynamic(
  () => import('../containers/VideoCallPage/VideoCallPage'),
  {
    ssr: false,
  }
);

export default function VideoCall() {
  return (
    <VideoCallProvider>
      <VideoCallPage />
    </VideoCallProvider>
  );
}
