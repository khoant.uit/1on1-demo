import '../styles/globals.css';
import 'antd/dist/antd.css';
import Head from 'next/head';
import { AuthAdminProvider } from 'context/auth-admin-context';
import { BookingInfoProvider } from 'context/booking-info-context';
import { ConfigProvider } from 'antd';
import jaJP from 'antd/lib/locale/ja_JP';

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta name='viewport' content='width=device-width, initial-scale=1' />
      </Head>
      <ConfigProvider locale={jaJP}>
        <AuthAdminProvider>
          <BookingInfoProvider>
            <Component {...pageProps} />
          </BookingInfoProvider>
        </AuthAdminProvider>
      </ConfigProvider>
    </>
  );
}

export default MyApp;
