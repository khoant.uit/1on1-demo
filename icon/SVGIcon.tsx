export const VoiceIcon = () => {
    return (
        <svg width={45} height={46} viewBox="0 0 45 46" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M11.9338 26.2908C11.9338 32.1614 16.6926 36.9201 22.5632 36.9201C28.4357 36.9201 33.1945 32.1614 33.1945 26.2908" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M33.1942 20.1869V15.9607C33.1942 10.09 28.4355 5.3313 22.563 5.3313C16.6923 5.3313 11.9336 10.09 11.9336 15.9607V20.1869" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M22.5632 40.9557V36.9207" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M9.18726 26.2908H35.9398" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
        </svg>
    )
}
export const MuteVoiceIcon = () => {
    return (
        <svg width={45} height={46} viewBox="0 0 45 46" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M11.934 26.6421C11.934 32.5127 16.6927 37.2715 22.5633 37.2715C28.4358 37.2715 33.1946 32.5127 33.1946 26.6421" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M33.1942 20.5382V16.312C33.1942 10.4414 28.4354 5.68262 22.5629 5.68262C16.6923 5.68262 11.9335 10.4414 11.9335 16.312V20.5382" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M22.5631 41.307V37.272" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M9.18726 26.6421H35.9398" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M6.15686 39.2896L40.8444 4.60205" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
        </svg>
    )
}
export const VideoIcon = () => {
    return (
        <svg width={45} height={46} viewBox="0 0 45 46" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M30.2839 18.7802C33.7304 16.048 38.3554 13.4052 39.2304 14.3516C40.6768 15.9052 40.5518 30.6909 39.2304 32.1016C38.4268 32.9766 33.7661 30.3337 30.2839 27.6195" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path fillRule="evenodd" clipRule="evenodd" d="M4.71448 23.213C4.71448 13.4773 7.94841 10.2327 17.6538 10.2327C27.3573 10.2327 30.5913 13.4773 30.5913 23.213C30.5913 32.9469 27.3573 36.1934 17.6538 36.1934C7.94841 36.1934 4.71448 32.9469 4.71448 23.213Z" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
        </svg>
    )
}

export const MuteVideoIcon = () => {
    return (
        <svg width={46} height={46} viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M31.2839 19.1315C34.7304 16.3993 39.3554 13.7565 40.2304 14.7029C41.6768 16.2565 41.5518 31.0422 40.2304 32.4529C39.4268 33.3279 34.7661 30.6851 31.2839 27.9708" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path fillRule="evenodd" clipRule="evenodd" d="M5.71448 23.5643C5.71448 13.8286 8.94841 10.584 18.6538 10.584C28.3573 10.584 31.5913 13.8286 31.5913 23.5643C31.5913 33.2983 28.3573 36.5447 18.6538 36.5447C8.94841 36.5447 5.71448 33.2983 5.71448 23.5643Z" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M1.58887 40.9089L36.2764 6.22144" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
        </svg>

    )
}

export const EndCallIcon = () => {
    return (
        <svg width={46} height={46} viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M11.627 25.1267C3.0283 15.0742 4.61866 10.257 5.81492 8.56505C5.99406 8.24656 10.3133 1.79708 14.9292 5.57918C24.0255 13.0837 16.8022 13.8003 18.1955 18.5578" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M21.8961 23.7329C31.1338 32.6308 29.9594 18.6171 39.2926 29.9435C43.0944 34.5816 36.6254 38.8812 36.3069 39.0604C34.4379 40.414 28.6039 42.2254 15.9248 29.7046" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M3.92346 40.9565L38.611 6.26904" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
        </svg>
    )
}

export const CameraIcon = () => {
    return (
        <svg width={45} height={45} viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd" clipRule="evenodd" d="M23.0489 37.8757C38.1338 37.8757 39.9298 33.356 39.9298 23.5626C39.9298 16.6981 39.0219 13.0251 33.3041 11.4461C32.7791 11.2803 32.1969 10.9645 31.7252 10.4455C30.9634 9.61059 30.4068 7.04678 28.5673 6.27112C26.7278 5.49744 19.3403 5.53296 17.5305 6.27112C15.7226 7.01125 15.1344 9.61059 14.3726 10.4455C13.9009 10.9645 13.3206 11.2803 12.7936 11.4461C7.07586 13.0251 6.16797 16.6981 6.16797 23.5626C6.16797 33.356 7.96402 37.8757 23.0489 37.8757Z" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M15.3696 19.4001C16.7229 16.1772 19.8691 14.1026 23.2522 14.1772C26.2501 14.2429 29.0044 15.9862 30.4362 18.7006" stroke="white" strokeWidth="1.5" strokeMiterlimit={10} strokeLinecap="round" />
            <path d="M32.2161 18.0697C32.4069 18.1533 32.4957 18.3699 32.4139 18.5541L31.4599 20.7248C31.3796 20.9091 31.1595 20.9906 30.9686 20.907L28.725 19.9206C28.5342 19.8369 28.4454 19.6204 28.5271 19.4362C29.7568 18.9802 30.9865 18.5243 32.2161 18.0697Z" fill="white" />
            <path d="M30.7271 25.4749C29.3738 28.6978 26.2276 30.7724 22.8445 30.6978C19.8465 30.6321 17.0923 28.8888 15.6605 26.1744" stroke="white" strokeWidth="1.5" strokeMiterlimit={10} strokeLinecap="round" />
            <path d="M13.8806 26.8053C13.6898 26.7217 13.601 26.5051 13.6827 26.3209L14.6367 24.1502C14.7171 23.9659 14.9372 23.8844 15.1281 23.968L17.3717 24.9544C17.5625 25.0381 17.6513 25.2546 17.5695 25.4388C16.3399 25.8948 15.1102 26.3507 13.8806 26.8053Z" fill="white" />
        </svg>
    )
}