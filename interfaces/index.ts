export interface RegisterFormData {
  time: string;
  type: 'first_time' | 'more_time';
  name: { first: string; last: string };
  tel: string;
  address: string;
  content: string;
}
export interface AuthAdmin {
  isAuthenticated: boolean;
  name: string;
}
export interface BookingInfo {
  [time: string]: RegisterFormData;
}
